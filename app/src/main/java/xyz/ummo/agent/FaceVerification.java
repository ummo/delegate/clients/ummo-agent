package xyz.ummo.agent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.VideoView;

public class FaceVerification extends AppCompatActivity  implements View.OnClickListener {

    ImageView swipeCamera, startVideo, flashLight;

    private String shippingCostBeforeInsurance;
    private double additionalCost;


    VideoView videoView;
    Uri videoFileUri;
    public static int VIDEO_CAPTURED = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_verification);

        swipeCamera = findViewById(R.id.swipe_camera_icon);
        startVideo = findViewById(R.id.capture_image_icon);
        flashLight = findViewById(R.id.flash_light_icon);

        swipeCamera.setOnClickListener(this);
        startVideo.setOnClickListener(this);
        flashLight.setOnClickListener(this);

        videoView = this.findViewById(R.id.VideoView);

    }



    @Override
    public void onClick(View v) {
        if (v == startVideo) {

            intentToDocVerificIntro();

        } else if (v == flashLight) {
            videoView.setVideoURI(videoFileUri);
            videoView.start();
        }
    }

    public void setShippingCostBeforeInsurance(String shippingCostBeforeInsurance) {
        this.shippingCostBeforeInsurance = shippingCostBeforeInsurance;
    }

    public void setAdditionalCost(double additionalCost) {
        this.additionalCost = additionalCost;
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == VIDEO_CAPTURED) {
            videoFileUri = data.getData();
        }
    }

    private void intentToDocVerificIntro(){


        Intent i = new Intent(this, DocumentVerificationIntro.class);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);

    }
}
