package xyz.ummo.agent.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Objects;

public class PrefManager {
    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private Context _context;
    private final String TAG = "PrefMan";

    // shared pref mode
    private int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "UMMO_AGENT_PREFERENCES";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }

    void unSetFirstTimeLaunch() {
        PreferenceManager
                .getDefaultSharedPreferences(this._context)
                .edit()
                .remove("jwt")
                .apply();
    }

    public boolean isFirstTimeLaunch() {
        return Objects.requireNonNull(PreferenceManager
                .getDefaultSharedPreferences(this._context)
                .getString("jwt", ""))
                .isEmpty();
    }
}
