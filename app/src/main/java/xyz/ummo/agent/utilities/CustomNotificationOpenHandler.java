package xyz.ummo.agent.utilities;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import xyz.ummo.agent.delegate.Agent;
import xyz.ummo.agent.delegate.AgentObject;
import xyz.ummo.agent.ui.DelegationChat;

public class CustomNotificationOpenHandler implements OneSignal.NotificationOpenedHandler {

    private static final String TAG = "CustomNotifOpenHandler";
    String destination, serviceId;
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        JSONObject data = result.notification.payload.additionalData;

        Log.e(TAG, "data ->"+data.toString());

        try {
            destination = data.getString("open_action");
            serviceId = data.getString("service_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (destination.equals("DelegationChat")){
            Log.e(TAG, "destination->"+destination);
            Intent intent = new Intent(AgentObject.INSTANCE.getAgent().getApplicationContext(), DelegationChat.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra("service_id",serviceId);
            AgentObject.INSTANCE.getAgent().startActivity(intent);
        }
    }
}
