package xyz.ummo.agent.utilities;

import androidx.annotation.NonNull;

public class ServiceProviderWithId{
    private String serviceProvider;
    private String objectId;

    public ServiceProviderWithId(String _serviceProvider, String _objectId){
        this.serviceProvider = _serviceProvider;
        this.objectId = _objectId;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    @NonNull
    @Override
    public String toString() {
        return serviceProvider;
    }
}
