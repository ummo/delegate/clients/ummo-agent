package xyz.ummo.agent.utilities;

public interface ServiceProviderWithIdInterface {
    String getServiceProvider();
    String getObjectId();

}
