package xyz.ummo.agent.utilities;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import xyz.ummo.agent.data.entity.AgentEntity;
import xyz.ummo.agent.data.repo.AppRepository;

public class AgentViewModel extends AndroidViewModel {
    private AppRepository appRepository;
    private LiveData<AgentEntity> agentEntityLiveData;

    public AgentViewModel(Application application){
        super(application);

        appRepository = new AppRepository(application);
        agentEntityLiveData = appRepository.getAgentEntityLiveData();
    }

    public LiveData<AgentEntity> getAgentEntityLiveData(){
        Log.e("AgentVM", "Agent from ViewModel->"+ agentEntityLiveData);
        return agentEntityLiveData;
    }

    public void setAgent(AgentEntity agentEntity){
        appRepository.insertAgent(agentEntity);
        Log.e("AgentVM", "INSERTED->"+agentEntity);
    }
}
