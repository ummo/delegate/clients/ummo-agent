package xyz.ummo.agent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class DocumentVerification extends AppCompatActivity implements View.OnClickListener{

    ImageView swipeCamera, startVideo, flashLight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document_verification);

        swipeCamera = findViewById(R.id.swipe_camera_icon);
        startVideo = findViewById(R.id.capture_image_icon);
        flashLight = findViewById(R.id.flash_light_icon);

        swipeCamera.setOnClickListener(this);
        startVideo.setOnClickListener(this);
        flashLight.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == startVideo) {

            intentToAddressVerificIntro();

        } else if (v == flashLight) {
        }
    }

    private void intentToAddressVerificIntro(){


        Intent i = new Intent(this, AddressVerificationIntro.class);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);

    }
}
