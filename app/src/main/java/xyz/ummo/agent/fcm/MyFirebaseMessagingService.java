package xyz.ummo.agent.fcm;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import xyz.ummo.agent.ui.MainActivity;
import xyz.ummo.agent.R;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFcmListener ";
    private static String click_action;
//    private final Context mContext = getApplicationContext();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if(remoteMessage.getData().size() > 0){
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());

            /*
             * Retrieving notification payload from OneSignal message:
             *   payload structure is based on OneSignal's API;
             *   the element we're looking for is bundled in a JSON-String hybrid object,
             *   so we dig deep and pull out the `a` field that contains the `click_action`.
             * */
            JSONObject payload = new JSONObject(remoteMessage.getData());
//            Log.e(TAG, "Payload->"+payload);
            try {
                String custom = payload.getString("custom");
//                Log.e(TAG, "custom->"+custom+"\n");
                JSONObject custom_object = new JSONObject(custom);
//                Log.e(TAG, "custom_object-> "+custom_object+"\n");

                JSONObject a = custom_object.getJSONObject("a");
//                Log.e(TAG, "a->"+a);
                click_action = a.getString("click_action");
                Log.e(TAG, "onGetData click_action->"+click_action);//GOT IT!
//                this.sendNotification("Title","Body",click_action);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e(TAG, "JSONException->",e);
            }
        }

        if(remoteMessage.getNotification() != null){
//            String image = remoteMessage.getNotification().getIcon();
            String title = remoteMessage.getNotification().getTitle();
            String text = remoteMessage.getNotification().getBody();
//            String sound = remoteMessage.getNotification().getSound();
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            Log.e(TAG, "Message Notification Data: " + remoteMessage.getData());
            Log.e(TAG+"receive", "onGetNotification click_action->"+click_action);

            assert click_action != null;
            this.sendNotification(title, text, click_action);
        }
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e(TAG, "New Token->"+s);
    }

    private void sendNotification(String title, String messageBody, String _click_action) {

        /*int id = 0;
        Object obj = remoteMessage.getData().get("id");
        if (obj != null) {
            id = Integer.valueOf(obj.toString());
        }*/

        Log.e(TAG+"send", "click_action value->"+_click_action);
        Intent intent;
        switch (_click_action) {
            case "RPT_ACTIVITY":
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Log.e(TAG+"send", "onSwitch click_action final->"+_click_action);
                break;
            /*case "HOME_ACTIVITY":
                intent = new Intent(this, BottomNavActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;
            case "DISCOVER_ACTIVITY":
                intent = new Intent(this, DiscoverFragment.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                break;*/
            default:
                intent = new Intent(this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Log.e(TAG+"send", "onSwitch click_action final->"+_click_action);
                break;
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /*Request code*/, intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = null;
        try {
            Log.e(TAG+"send", "onBuilder click_action final->"+_click_action);
            notificationBuilder = new NotificationCompat.Builder(this, "Main_Channel_ID")
                    .setSmallIcon(R.drawable.logo)
                    .setContentTitle(URLDecoder.decode(title, "UTF-8"))
                    .setContentText(URLDecoder.decode(messageBody, "UTF-8"))
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setContentIntent(pendingIntent);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (notificationBuilder != null) {
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert notificationManager != null;
            notificationManager.notify(0, notificationBuilder.build());
            Log.e(TAG+"send", "onBuilder click_action final->"+_click_action);
        } else {
            Log.e(TAG+"send", "Something random!!!");
        }
    }
}
