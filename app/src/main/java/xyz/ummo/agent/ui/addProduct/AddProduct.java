package xyz.ummo.agent.ui.addProduct;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;

import xyz.ummo.agent.R;
import xyz.ummo.agent.data.entity.ProductEntity;
import xyz.ummo.agent.delegate.CreateProduct;
import xyz.ummo.agent.ui.MainActivity;
import xyz.ummo.agent.utilities.ServiceProviderWithId;
import xyz.ummo.agent.utilities.ServiceProviderWithIdInterface;

import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.chip.ChipGroup;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddProduct extends AppCompatActivity implements ServiceProviderWithIdInterface {
    private Toolbar toolbar;
    private Menu menu;
    private static final String TAG = "AddProduct";
    private String serviceProviderValue;
    public static final int ADD_PRODUCT_ATTRIBUTE = 1;
    private AddProductViewModel addProductViewModel;
    private String[] serviceProviderNames, serviceProviderIds;
    private ArrayList <String> documentsArrayList = new ArrayList<>();
    private ArrayList <String> stepsArrayList = new ArrayList<>();
    private ArrayList <ServiceProviderWithId> serviceProviders = new ArrayList<>();
    private Map<String,String> providers = new HashMap<>();
    private Spinner serviceProviderSpinner;
    private ArrayAdapter<ServiceProviderWithId> serviceProviderAdapter;
    private ArrayAdapter<String> docsListAdapter, stepsListAdapter;
    private TextView providerTxt;
    private ImageView addAnotherDocImageView, addAnotherStepImageView;
    private ListView documentsListView, stepsListView;
    private EditText productNameTxt, descriptionTxt, documentTxt, costTxt, durationTxt, stepsTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_service);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Add Product");

        productNameTxt = findViewById(R.id.product_name_edit_text);
        descriptionTxt = findViewById(R.id.product_description_edit_text);
        providerTxt = findViewById(R.id.service_provider_text_view);
        documentTxt = findViewById(R.id.add_product_documents_edit_text);
        costTxt = findViewById(R.id.product_cost_edit_text);
        durationTxt = findViewById(R.id.product_duration_edit_text);
        stepsTxt = findViewById(R.id.add_product_steps_edit_text);
        addAnotherDocImageView = findViewById(R.id.add_another_document_image_view);
        addAnotherStepImageView = findViewById(R.id.add_another_step_image_view);
        documentsListView = findViewById(R.id.documents_list_view);
        stepsListView = findViewById(R.id.steps_list_view);
        serviceProviderSpinner = findViewById(R.id.service_provider_spinner);

        getServiceProviders();
//        serviceProviderAdapter = new ArrayAdapter<>(this, R.layout.spinner_text_view,R.id.spinner_text_view,serviceProviderNames);
        serviceProviderAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, serviceProviders);
        docsListAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, documentsArrayList);
        stepsListAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, stepsArrayList);
        documentsListView.setAdapter(docsListAdapter);
        stepsListView.setAdapter(stepsListAdapter);
//        serviceProviderAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        Log.e(TAG, "onCreate: getServiceProviders ->"+ Arrays.toString(serviceProviderNames));
        serviceProviderSpinner.setAdapter(serviceProviderAdapter);
        addListenerOnSpinnerItemSelection();
        addAnotherDocument();
        addAnotherStep();
    }

    public void addListenerOnSpinnerItemSelection(){
        serviceProviderSpinner = findViewById(R.id.service_provider_spinner);
        serviceProviderSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                serviceProviderValue = serviceProviders.get(position).getObjectId();
                Log.e(TAG, "onCreate: selected Item->"+serviceProviders.get(position).getObjectId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.e(TAG, "onCreate: Nothing Selected!");
            }
        });
    }

    private void addAnotherDocument(){
        addAnotherDocImageView = findViewById(R.id.add_another_document_image_view);
        documentsListView = findViewById(R.id.documents_list_view);

        addAnotherDocImageView.setOnClickListener(v -> {
        documentsArrayList.add(documentTxt.getText().toString());
        if (documentsArrayList.size()!=0){
            documentTxt.getText().clear();
            docsListAdapter.notifyDataSetChanged();
            documentsListView.scrollTo(0, documentsListView.getFirstVisiblePosition());
        }
        Log.e(TAG, "addAnotherDoc: documentEditText->"+ documentsArrayList);
    });
    }

    private void addAnotherStep(){
        addAnotherStepImageView = findViewById(R.id.add_another_step_image_view);
        stepsListView = findViewById(R.id.steps_list_view);

        addAnotherStepImageView.setOnClickListener(v -> {

        stepsArrayList.add(stepsTxt.getText().toString());
        if (stepsArrayList.size() != 0) {
            stepsTxt.getText().clear();
            stepsListAdapter.notifyDataSetChanged();
            stepsListView.scrollTo(0, stepsListView.getLastVisiblePosition());
//                Log.e(TAG, "addAnotherStep: stepsEditText->");
        }
    });
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_attribute, menu);
        this.menu = menu;
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
            return  true;

            case R.id.action_save:
            saveProduct();
            return true;

            default:
            return super.onOptionsItemSelected(item);
        }
    }

    public void getServiceProviders(){
        String listString = PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString("providers","[]");

        try {
            JSONArray arr = new JSONArray(listString);
            serviceProviderNames = new String[arr.length()];
            serviceProviderIds = new String[arr.length()];
            for (int i=0; i<arr.length();i++){
                providers.put(arr.getJSONObject(i).getString("service_name"),arr.getJSONObject(i).getString("_id"));
                serviceProviderNames[i] = arr.getJSONObject(i).getString("service_name");
                serviceProviderIds[i] = arr.getJSONObject(i).getString("_id");
                serviceProviders.add(new ServiceProviderWithId(serviceProviderNames[i],serviceProviderIds[i]));
//                Log.e(TAG, "getServiceProviders ->"+ serviceProviders.get(i).getServiceProvider());
            }
//            Log.e(TAG, "getServiceProviders ->"+ Arrays.toString(serviceProviderIds));
        }catch (JSONException jse){
            Log.e(TAG, "Providers failed to load");
            Toast.makeText(this, "Providers failed to load!",Toast.LENGTH_LONG).show();
        }
    }

    private void saveProduct(){
        String productName = productNameTxt.getText().toString();
        String productDescription = descriptionTxt.getText().toString();
        String productProvider = serviceProviderValue;
        String productCost = costTxt.getText().toString();
        String productDuration = durationTxt.getText().toString();

        if (productName.trim().isEmpty() ) {
            Toast.makeText(this, "Please provide a Product Name", Toast.LENGTH_SHORT).show();
            return;
        }
        if (productDescription.trim().isEmpty()){
            Toast.makeText(this, "Please provide a Product Description", Toast.LENGTH_SHORT).show();
            return;
        }
        if (productProvider.trim().isEmpty()){
            Toast.makeText(this, "Please provide a Product Provider", Toast.LENGTH_SHORT).show();
            return;
        }
        if (documentsArrayList.size()==0){
            Toast.makeText(this, "Please provide Documents Required", Toast.LENGTH_SHORT).show();
            return;
        }
        if (productCost.trim().isEmpty()){
            Toast.makeText(this, "Please provide a Product Cost", Toast.LENGTH_SHORT).show();
            return;
        }
        if (productDuration.trim().isEmpty()){
            Toast.makeText(this, "Please provide a Duration", Toast.LENGTH_SHORT).show();
            return;
        }
        if (stepsArrayList.size()==0){
            Toast.makeText(this, "Please provide Steps", Toast.LENGTH_SHORT).show();
            return;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.e(TAG, "saveProduct TIMEOUT");

            }
        }, 5000);

        ProgressDialog progress = new ProgressDialog(AddProduct.this);
        progress.setMessage("Saving Product...");
        progress.show();

        Log.e(TAG, "saveProduct: Service Provider->"+productProvider);

        ProductEntity productEntity =
        new ProductEntity(productName,
        productDescription,
        productProvider,
        documentsArrayList,
        productCost,
        productDuration,
        stepsArrayList);
        AddProductViewModel addProductViewModel = ViewModelProviders.of(this).get(AddProductViewModel.class);
        addProductViewModel.insertProduct(productEntity);
        //ON-SAVE
        new CreateProduct(
                productNameTxt.getText().toString(),
        descriptionTxt.getText().toString(),
        stepsArrayList,
        false,
        costTxt.getText().toString(),
        documentsArrayList,
        productProvider,
        durationTxt.getText().toString()){

        @Override
        public void done(@NotNull byte[] data, @NotNull Number code) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(AddProduct.this, MainActivity.class));
                    progress.dismiss();
                    finish();
                }
            });
        }
    };
    }

    @Override
    protected void onDestroy() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.edit()
                .remove("service name")
                .remove("description")
                .remove("provider")
                .remove("documents")
                .remove("cost")
                .remove("duration")
                .remove("steps")
                .apply();

        super.onDestroy();
    }

    @Override
    public String getServiceProvider() {
        return null;
    }

    @Override
    public String getObjectId() {
        return null;
    }
}


