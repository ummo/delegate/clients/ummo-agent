package xyz.ummo.agent.ui.signup

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.RelativeLayout
import android.widget.TextView
import android.widget.Toast

import com.alimuzaffar.lib.pin.PinEntryEditText
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.hbb20.CountryCodePicker

import java.util.Objects
import java.util.concurrent.TimeUnit
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.firebase.auth.*
import com.onesignal.OneSignal
import org.json.JSONObject
import timber.log.Timber
import xyz.ummo.agent.utilities.PrefManager
import xyz.ummo.agent.R
import xyz.ummo.agent.delegate.Login
import xyz.ummo.agent.ui.LoginActivity
import xyz.ummo.agent.ui.MainActivity

class SlideIntro : AppCompatActivity(), View.OnClickListener {

    private var viewPager: ViewPager? = null
    private var myViewPagerAdapter: MyViewPagerAdapter? = null
    private var dotsLayout: LinearLayout? = null
    private var layouts: IntArray? = null
    private var btnNext: Button? = null
    private var signUpButton: Button? = null
    private var resendCodeButton: TextView? = null
    private val launchLoginTextView: TextView? = null
    private var prefManager: PrefManager? = null
    private var agentNameField: EditText? = null
    private var agentContactField: EditText? = null
    private var agentEmailField: EditText? = null
    private var agentPasswordField: EditText? = null
    private var progressBar: ProgressBar? = null
    private var registrationCcp: CountryCodePicker? = null
    private var agentContact: String = ""
    private var pinEntryEditText: PinEntryEditText? = null
    private var agentName: String = ""
    private var agentEmail: String = ""
    private var agentPassword: String = ""
    private var playerId: String = " "
    private var registrationLayout: RelativeLayout? = null
    private var isClear: Boolean = false
    private var isValid: Boolean? = false
    private var mAuth: FirebaseAuth? = null
    private var verificationCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks? = null
    private var mVerificationInProgress = false
    private var autoVerified = false
    private var phoneVerificationId: String? = null
    private var resendToken: PhoneAuthProvider.ForceResendingToken? = null
    private val ummoAgentPreferences: String? = "UMMO_AGENT_PREFERENCES"
    private val mode = Activity.MODE_PRIVATE

    private val item: Int
        get() = viewPager!!.currentItem + 1

    //  viewpager change listener
    private var viewPagerPageChangeListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {

        override fun onPageSelected(position: Int) {

            addBottomDots(position)

            // changing the next button text 'NEXT' / 'GOT IT'
            when (position) {
                layouts!!.size - 1 -> {
                    btnNext!!.visibility = View.GONE
                    signUpClick()
                    Timber.e("onPageSelected position->$position")
                    //btnSkip.setVisibility(View.GONE);
                }
                1 -> {
                    btnNext!!.text = getString(R.string.verify)
                }
                else -> {
                    // still pages are left
                    btnNext!!.text = getString(R.string.next)
                    Timber.e("onPageSelected pos->$position")
                }
            }
        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {

        }

        override fun onPageScrollStateChanged(arg0: Int) {

        }
    }

    private fun checkForNameAndNumber(v: View): Int {
        agentName = myViewPagerAdapter!!.agentName
        agentContact = myViewPagerAdapter!!.contactCorrectnessCheck()

        if (agentName.isEmpty() || agentName.length < 3) {
            showError(agentNameField!!, "Your name should have at least 3 letters!")
            return 0
        } else if (agentContact.isEmpty()) {
            showError(agentContactField!!, "Please use a valid contact")
            return 0
        } else {
            saveAgentInformation() //Saving on firebase
            agentNameField!!.error = null
            sendCode(v)
            viewPager!!.setCurrentItem(1, true)
            return 1
        }
    }

    private fun checkEmailAndPassword(): Boolean {
        agentEmailField = findViewById(R.id.agentEmailEditText)
        agentPasswordField = findViewById(R.id.agentPasswordEditText)
        agentPassword = agentPasswordField!!.text.toString()
        agentEmail = agentEmailField!!.text.toString()

        val status = OneSignal.getPermissionSubscriptionState()
        val oneToken = status.subscriptionStatus.pushToken

        playerId = status.subscriptionStatus.userId
        Timber.e( "OneSignal PlayerId->$playerId")

        if (agentPassword.length < 6) {
            showError(agentPasswordField!!, "Try a stronger/longer password")
            return false
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(agentEmail).matches()) {
            showError(agentEmailField!!, "Please use a valid email...")
            return false
        }
        return true
    }

    private fun signUpClick() {

        signUpButton = findViewById(R.id.sign_up_btn)
        signUpButton!!.setOnClickListener {

            if (checkEmailAndPassword()) {
                val progress = ProgressDialog(this@SlideIntro)
                progress.setMessage("Signing up...")
                progress.show()
                object : Login(applicationContext, agentName, agentEmail, agentPassword, agentContact, playerId) {
                    override fun done(data: ByteArray, code: Number) {

                        if (code == 200) {
                            progress.dismiss()
                            launchHomeScreen()
                            //TODO: save agent info to prefs here
                            val sharedPreferences = getSharedPreferences(ummoAgentPreferences, mode)
                            val editor: SharedPreferences.Editor
                            editor = sharedPreferences.edit()
                            editor.putBoolean("SIGNED_UP", true)
                            editor.putString("AGENT_NAME", agentName)
                            editor.putString("AGENT_CONTACT", agentContact)
                            editor.putString("AGENT_EMAIL", agentEmail)
                            editor.putString("AGENT_PID", playerId)
                            Timber.e("SignUp Result - String%s", String(data))
                            val dataObject = JSONObject(String(data))
                            Timber.e("SignUp Result - JSON ->%s", dataObject.getString("_id"))
                            editor.putString("AGENT_ID", dataObject.getString("_id"))
                            editor.apply()
                        } else {
                            Log.e("Error", "Something happened $code")
                            Toast.makeText(this@SlideIntro, "Something went Awfully bad", Toast.LENGTH_LONG).show()
                            //Show an error
                        }
                    }
                }
            }

        }
    }

    private fun showError(view: EditText, error: String) {
        view.error = error
        view.requestFocus()
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Checking for first time launch - before calling setContentView()
        Log.e(TAG, "OnCreate")
        prefManager = PrefManager(this)
        Log.e("$TAG IS-FIRST", "" + prefManager!!.isFirstTimeLaunch)
        if (!prefManager!!.isFirstTimeLaunch) {
            launchHomeScreen()
            finish()
        }

        setContentView(R.layout.activity_slide_intro)

        viewPager = findViewById(R.id.view_pager)
        dotsLayout = findViewById(R.id.layoutDots)
        btnNext = findViewById(R.id.btn_next)
        progressBar = findViewById(R.id.registration_loader)

        layouts = intArrayOf(R.layout.register_slide, R.layout.confirm_registration_slide, R.layout.sign_up_slide)

        // adding bottom dots
        addBottomDots(0)

        // making notification bar transparent
        //changeStatusBarColor();

        myViewPagerAdapter = MyViewPagerAdapter()
        viewPager!!.adapter = myViewPagerAdapter
        viewPager!!.addOnPageChangeListener(viewPagerPageChangeListener)

        //Assigning mAuth to FirebaseAuth instance
        mAuth = FirebaseAuth.getInstance()

        btnNext!!.setOnClickListener(View.OnClickListener { v ->
            // checking for last page
            // if last page home screen will be launched

            when (layouts!![viewPager!!.currentItem]) {
                R.layout.register_slide -> {
                    checkForNameAndNumber(v)
                }

                R.layout.confirm_registration_slide -> {
                    verifyCode(v)
                }
            }

            val current = item

        })
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        //        Objects.requireNonNull(mapFragment).getMapAsync(this);
        //TODO: Add Android App Links
        // ATTENTION: This was auto-generated to handle app links.
        val appLinkIntent = intent
        val appLinkAction = appLinkIntent.action
        val appLinkData = appLinkIntent.data
    }

    private fun saveAgentInformation() {
        val displayName = agentNameField!!.text.toString()
        val agent = mAuth!!.currentUser

        if (displayName.isEmpty()) {
            agentNameField!!.error = "Please provide a name!"
            agentNameField!!.requestFocus()
            return
        }

        if (agent != null) {
            try {
                val profileChangeRequest = UserProfileChangeRequest.Builder().setDisplayName(displayName).build()
                agent.updateProfile(profileChangeRequest)
                        .addOnCompleteListener { task ->
                            if (task.isSuccessful) {
                                Toast.makeText(applicationContext, "Almost there, $displayName",
                                        Toast.LENGTH_LONG)
                                        .show()
                                Log.e(TAG, "saveAgentInfo Agent->$displayName")
                            }
                        }
            } catch (e: Exception) {
                Toast.makeText(applicationContext, "Error: " + e.message,
                        Toast.LENGTH_LONG)
                        .show()
                Log.e(TAG, "saveAgentInfo Error->" + e.message)
                e.printStackTrace()
            }

        }
    }

    override fun onClick(view: View) {
        myViewPagerAdapter = MyViewPagerAdapter()
        viewPager!!.adapter = myViewPagerAdapter
        viewPager!!.addOnPageChangeListener(viewPagerPageChangeListener)

        //Assigning mAuth to FirebaseAuth instance
        mAuth = FirebaseAuth.getInstance()
        Log.e("$TAG onClick", "ViewPager is ->" + viewPager!!.currentItem)

        when (view.id) {
            R.id.btn_next -> Log.e("$TAG onClick", "ViewPager is ->" + viewPager!!.currentItem)

            R.id.textViewLogin -> {
                finish()
                launchLoginActivity(view)
                Log.e("$TAG onDefault", "ViewPager is ->" + viewPager!!.currentItem)
            }

            else -> Log.e("$TAG onDefault", "ViewPager is ->" + viewPager!!.currentItem)
        }
    }

    private fun setUpVerificationCallbacks() {
        //[START initialize_auth]
        verificationCallbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.e(TAG, "onVerificationCompleted:$credential")
                // [START_EXCLUDE silent]
                mVerificationInProgress = false
                // [END_EXCLUDE]

                // [START_EXCLUDE silent]
                // Update the UI and attempt sign in with the phone credential
                //updateUI(STATE_VERIFY_SUCCESS, credential);
                // [END_EXCLUDE]
                signInWithPhoneAuthCredential(credential)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.e(TAG, "onVerificationFailed", e)
                // [START_EXCLUDE silent]
                mVerificationInProgress = false
                // [END_EXCLUDE]

                if (e is FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // [START_EXCLUDE]
                    agentContactField!!.error = "Invalid phone number."
                    // [END_EXCLUDE]
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // [START_EXCLUDE]
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show()
                    // [END_EXCLUDE]
                }
                // Show a message and update the UI
                // [START_EXCLUDE]
                //updateUI(STATE_VERIFY_FAILED);
                // [END_EXCLUDE]
            }

            override fun onCodeSent(verificationId: String, p1: PhoneAuthProvider.ForceResendingToken) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.e(TAG, "onCodeSent: verificationID -> $verificationId")
                Log.e(TAG, "onCodeSent: token-> $p1")

                // Save verification ID and resending token so we can use them later
                phoneVerificationId = verificationId
                resendToken = p1

//                resendCodeButton!!.isEnabled = true
            }
        }
        // [END phone_auth_callbacks]
    }

    private fun sendCode(view: View) {
        setUpVerificationCallbacks()
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                agentContact,
                60,
                TimeUnit.SECONDS,
                this,
                verificationCallbacks!!)
        mVerificationInProgress = true
    }

    fun resendCode(view: View) {

        val phoneNumber = registrationCcp!!.fullNumberWithPlus

        setUpVerificationCallbacks()

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,
                60,
                TimeUnit.SECONDS,
                this,
                verificationCallbacks!!,
                resendToken)
        Log.e(TAG, "resendCode called on contact->$phoneNumber")
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun verifyCode(view: View) {

        val code = Objects.requireNonNull(pinEntryEditText!!.text).toString()

        //TODO: handle this issue (KotlinNullPointerException)
        val credential = PhoneAuthProvider.getCredential(phoneVerificationId!!, code)
        signInWithPhoneAuthCredential(credential)
        Log.e(TAG, "verifyCode called on code->$code")
        Snackbar.make(findViewById(android.R.id.content), "Verifying code...",
                Snackbar.LENGTH_SHORT).show()
    }

    // [START sign_in_with_phone]
    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        //Sign in success, update UI with signed-in user info
                        val firebaseAgent = Objects.requireNonNull(task.result)?.user
                        Log.e(TAG, "signInWithCredential: Success! Agent->$firebaseAgent")
                        autoVerified = true
                        // [START_EXCLUDE]
                        //TODO: Handle UX for verification
                        Snackbar.make(findViewById(android.R.id.content), "Auto Verifying....",
                                Snackbar.LENGTH_SHORT).show()

                        viewPager!!.setCurrentItem(2, true)

                    } else {
                        // Sign in failed, display a message & update the UI
                        Log.e(TAG, "signInWithCredential: Failure", task.exception)

                        if (task.exception is FirebaseAuthInvalidCredentialsException) {
                            // The verification code entered was invalid
                            // [START_EXCLUDE silent]
                            Snackbar.make(findViewById(android.R.id.content), "Verification failed: " + task.exception!!,
                                    Snackbar.LENGTH_SHORT).show()
                            // [END_EXCLUDE]
                        }
                        // [START_EXCLUDE silent]
                        // Update UI
                        //                            updateUI(STATE_SIGNIN_FAILED);
                        // [END_EXCLUDE]
                    }
                }
    }
    // [END sign_in_with_phone]

    fun buildActionCodeSettings() {
        // [START auth_build_action_code_settings]
        val actionCodeSettings = ActionCodeSettings.newBuilder()
                .setUrl("https://app.ummo.xyz/finishSignUp?agent")
                .setHandleCodeInApp(true)
                .setIOSBundleId("xyz.ummo.agent.ios")
                .setAndroidPackageName(
                        "xyz.ummo.agent",
                        true, /* installIfNotAvailable */
                        "16"    /* minimumVersion */)
                .build()
        // [END auth_build_action_code_settings]
    }

    fun sendEmailVerification() {
        // [START send_email_verification]
        val auth = FirebaseAuth.getInstance()
        val user = auth.currentUser

        user?.sendEmailVerification()?.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Log.d(TAG, "Email sent.")
            }
        }
        // [END send_email_verification]
    }

    /**
     * Do not pass the user’s email in the redirect URL parameters
     * and re-use it as this may enable session injections.
     */

    fun sendSignInLink(email: String?, actionCodeSettings: ActionCodeSettings) {
        // [START auth_send_sign_in_link]
        val auth = FirebaseAuth.getInstance()

        auth.sendSignInLinkToEmail(email!!, actionCodeSettings)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.e(TAG, "sendSignInLink Email sent!")
                    } else {
                        Log.e(TAG, "sendSignInLink Error" + task.exception!!)
                    }
                }
        // [END auth_send_sign_in_link]
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun verifySignInLink() {
        // [START auth_verify_sign_in_link]
        val auth = FirebaseAuth.getInstance()
        val intent = intent
        val emailLink = Objects.requireNonNull(intent.data).toString()

        // Confirm the link is a sign-in with email link.
        if (auth.isSignInWithEmailLink(emailLink)) {
            // Retrieve this from wherever you stored it
            val email = "rego@ummo.xyz"

            // The client SDK will parse the code from the link for you.
            auth.signInWithEmailLink(email, emailLink)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Log.e(TAG, "Successfully signed in with email link!")
                            val result = task.result
                            // You can access the new user via result.getUser()
                            // Additional user info profile *not* available via:
                            // result.getAdditionalUserInfo().getProfile() == null
                            // You can check if the user is new or existing:
                            // result.getAdditionalUserInfo().isNewUser()
                            if (result != null) {
                                Log.e(TAG, "verifySignInLink Result is->" + result.user?.email!!)
                            }
                        } else {
                            Log.e(TAG, "Error signing in with email link", task.exception)
                        }
                    }
        }
        // [END auth_verify_sign_in_link]
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun linkWithSignInLink(email: String, emailLink: String) {
        val auth = FirebaseAuth.getInstance()

        // [START auth_link_with_link]
        // Construct the email link credential from the current URL.
        val credential = EmailAuthProvider.getCredentialWithLink(email, emailLink)

        // Link the credential to the current user.
        Objects.requireNonNull(auth.currentUser)?.linkWithCredential(credential)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Log.e(TAG, "Successfully linked emailLink credential!")
                        val result = task.result
                        // You can access the new user via result.getUser()
                        // Additional user info profile *not* available via:
                        // result.getAdditionalUserInfo().getProfile() == null
                        // You can check if the user is new or existing:
                        // result.getAdditionalUserInfo().isNewUser()
                        if (result != null) {
                            Log.e(TAG, "linkWithSignInLink Result is->" + result.user?.email!!)
                        }
                    } else {
                        Log.e(TAG, "Error linking emailLink credential", task.exception)
                    }
                }
        // [END auth_link_with_link]
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun reauthWithLink(email: String, emailLink: String) {
        val auth = FirebaseAuth.getInstance()

        // [START auth_reauth_with_link]
        // Construct the email link credential from the current URL.
        val credential = EmailAuthProvider.getCredentialWithLink(email, emailLink)

        // Re-authenticate the user with this credential.
        Objects.requireNonNull(auth.currentUser)?.reauthenticateAndRetrieveData(credential)
                ?.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        // User is now successfully reauthenticated
                        Log.e(TAG, "reAuthWithLink User is now successfully reAuthed->" + auth.currentUser!!.email!!)
                    } else {
                        Log.e(TAG, "Error re-authenticating", task.exception)
                    }
                }
        // [END auth_reauth_with_link]
    }

    private fun addBottomDots(currentPage: Int) {
        val dots = arrayOfNulls<TextView>(layouts!!.size)

        val colorsActive = resources.getIntArray(R.array.array_dot_active)
        val colorsInactive = resources.getIntArray(R.array.array_dot_inactive)

        dotsLayout!!.removeAllViews()
        for (i in dots.indices) {
            dots[i] = TextView(this)
            dots[i]!!.text = Html.fromHtml("&#8226;")
            dots[i]!!.textSize = 35f
            dots[i]!!.setTextColor(colorsInactive[currentPage])
            dotsLayout!!.addView(dots[i])
        }

        if (dots.size > 0)
            dots[currentPage]!!.setTextColor(colorsActive[currentPage])
    }

    private fun launchHomeScreen() {
        startActivity(Intent(this@SlideIntro, MainActivity::class.java))
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out)
        finish()
    }

    fun launchLoginActivity(view: View) {
        startActivity(Intent(this@SlideIntro, LoginActivity::class.java))
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out)
        Log.e(TAG, "launchLogInScreen successfully!$view")
        finish()
    }

    inner class MyViewPagerAdapter internal constructor() : PagerAdapter() {
        private var layoutInflater: LayoutInflater? = null

        internal val agentName: String
            get() = if (agentNameField != null) {
                agentNameField!!.text.toString()
            } else {
                ""
            }

        internal/*if (!Patterns.EMAIL_ADDRESS.matcher(agentEmailField.getText()).matches()){
                    agentEmailField.setError("Please use a valid email");
                    agentEmailField.requestFocus();
                }*/ val agentEmail: String
            get() {
                return if (agentEmailField != null) {
                    Log.e("$TAG adapter", "getAgentEmail: has email -> Yes")

                    agentEmailField!!.text.toString().trim { it <= ' ' }
                } else {
                    Log.e("$TAG adapter", "getAgentEmail: has email -> No")

                    ""
                }
            }

        internal val agentPassword: String
            get() {
                return if (agentPasswordField != null) {
                    Log.e("$TAG adapter", "getAgentPass -> YES!")
                    agentPasswordField!!.text.toString()
                } else {
                    Log.e("$TAG adapter", "getAgentPass -> NO!")
                    ""
                }
            }

        internal fun contactCorrectnessCheck(): String {
            //TODO Add country input
            registrationCcp!!.registerCarrierNumberEditText(agentContactField)
            return if (registrationCcp!!.isValidFullNumber) {
                registrationCcp!!.fullNumberWithPlus
            } else {
                ""
            }
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            registrationLayout = findViewById(R.id.registration_relative_layout)

            agentNameField = findViewById(R.id.agentNameEditText)
            agentContactField = findViewById(R.id.agentContactEditText)

            //launchLoginTextView = findViewById(R.id.textViewLogin);
            agentEmailField = findViewById(R.id.agentEmailEditText)

            registrationCcp = findViewById(R.id.registration_ccp)

            pinEntryEditText = findViewById(R.id.txt_pin_entry)

            resendCodeButton = findViewById(R.id.resend_btn)

            val view = layoutInflater!!.inflate(layouts!![position], container, false)
            container.addView(view)

            return view
        }

        override fun getCount(): Int {
            return layouts!!.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }

    companion object {
        private const val TAG = "SlideInto"
    }
}
