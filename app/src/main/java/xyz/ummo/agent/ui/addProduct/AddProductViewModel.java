package xyz.ummo.agent.ui.addProduct;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import xyz.ummo.agent.data.entity.ProductEntity;
import xyz.ummo.agent.data.model.Product;
import xyz.ummo.agent.data.repo.AppRepository;

public class AddProductViewModel extends AndroidViewModel {
    private AppRepository appRepository;
    private LiveData<ProductEntity> productEntityLiveData;

    public AddProductViewModel(@NonNull Application application){
        super(application);

        appRepository = new AppRepository(application);
        productEntityLiveData = appRepository.getProductEntityLiveData();
    }

    public void insertProduct(ProductEntity productEntity){
        appRepository.insertProduct(productEntity);
    }

    public void updateProduct(ProductEntity productEntity){
        appRepository.updateProduct(productEntity);
    }

    public void deleteAllProducts(){
        appRepository.deleteAllProducts();
    }

    public LiveData<ProductEntity> getProductEntityLiveData(){
        return productEntityLiveData;
    }
}
