package xyz.ummo.agent.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.jetbrains.annotations.NotNull;

import xyz.ummo.agent.R;
import xyz.ummo.agent.delegate.Login;
import xyz.ummo.agent.ui.signup.SlideIntro;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    private static final String TAG = "LoginActivity";
    private EditText emailLogin, passwordLogin;
    private Button loginButton;
    private TextView toSignUpTextView;
    private ProgressBar loginProgress;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseUser mCurrentAgent;
    private String agentId;
    private final int mode = Activity.MODE_PRIVATE;
    private final String loginPrefs = "UMMO_AGENT_PREFERENCES";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        emailLogin = findViewById(R.id.editTextEmail);
        passwordLogin = findViewById(R.id.editTextPassword);
        loginButton = findViewById(R.id.buttonLogin);
        toSignUpTextView = findViewById(R.id.textViewSignup);
        loginProgress = findViewById(R.id.progressbar);

        mAuth = FirebaseAuth.getInstance();

        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        mAuth = FirebaseAuth.getInstance();
        Log.e(TAG + " onClick", "Implementing onClick cases...");

        switch (view.getId()){
            case R.id.buttonLogin:
                Log.e(TAG + " onClick", "Switching to ButtonLogin");
                agentLogin(view);
                break;

            case R.id.textViewSignup:
                Log.e(TAG + " onClick", "Switching to SignUpTextView");
                launchSignUpActivity(view);
                break;

            default:
                break;
        }
    }

    public void launchSignUpActivity(View view){
        startActivity(new Intent(LoginActivity.this, SlideIntro.class));
        finish();
    }

    public void agentLogin(View view){
        String email = emailLogin.getText().toString().trim();
        String password = passwordLogin.getText().toString().trim();

        if (email.isEmpty()){
            emailLogin.setError("Email is required.");
            emailLogin.requestFocus();
            return;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            emailLogin.setError("Please enter a valid email.");
            emailLogin.requestFocus();
            return;
        }

        if (password.isEmpty()) {
            passwordLogin.setError("Incorrect password. Please try again.");
            passwordLogin.requestFocus();
            return;
        }

        if (password.length() <6){
            passwordLogin.setError("Tip: Minimum length should be at least 6");
            passwordLogin.requestFocus();
            return;
        }

        loginProgress.setVisibility(View.VISIBLE);

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(task -> {

                    SharedPreferences loginPreferences = getSharedPreferences(loginPrefs, mode);
                    String agentName = loginPreferences.getString("AGENT_NAME","");
                    String agentContact = loginPreferences.getString("AGENT_CONTACT", "");
                    String agentPid = loginPreferences.getString("AGENT_PID", "");

                    Log.e(TAG, "onLoginComplete agentName->"+agentName+ " agentContact->"+agentContact);

                    if (agentName != null && agentContact != null) {
                        new Login(LoginActivity.this,agentName,email,password,agentContact, agentPid){
                            @Override
                            public void done(@NotNull byte[] data, @NotNull Number code) {
                                Log.e("Login Result",new String(data));
                            }
                        };
                    }
                    loginProgress.setVisibility(View.GONE);

                    if (task.isSuccessful()){
                        launchHomeScreen();
                        getCurrentAgent();
                        Log.e(TAG, "signInWithEmailPassword Agent->"+getCurrentAgent());
                        Toast.makeText(getApplicationContext(), "Welcome back "+ agentName,
                                Toast.LENGTH_LONG)
                                .show();
                    } else {
                        Toast.makeText(getApplicationContext(), task.getException().getMessage(),
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                });
    }

    private void launchHomeScreen() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
        finish();
    }

    public String getCurrentAgent(){
        mAuthListener = new FirebaseAuth.AuthStateListener(){

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser agent = firebaseAuth.getCurrentUser();
                if (agent != null){
                    agentId = mCurrentAgent.getUid();
                    mCurrentAgent = agent;
                    Toast.makeText(getApplicationContext(), "Welcome Agent " + agentId,
                            Toast.LENGTH_LONG)
                            .show();
                }
            }
        };
        return agentId;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mAuth.getCurrentUser() != null){
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }
    }
}
