package xyz.ummo.agent.ui

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.navigation.NavigationView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.auth.FirebaseUser
import com.onesignal.OneSignal
import io.sentry.core.Sentry
import timber.log.Timber
import xyz.ummo.agent.R
import xyz.ummo.agent.data.entity.AgentEntity
import xyz.ummo.agent.delegate.GetPublicServices
import xyz.ummo.agent.delegate.Logout
import xyz.ummo.agent.delegate.SocketIO
import xyz.ummo.agent.ui.MainActivity
import xyz.ummo.agent.ui.addProduct.AddProduct
import xyz.ummo.agent.ui.delegatedService.DelegatedServiceFragment
import xyz.ummo.agent.ui.selfDelegate.SelfDelegateFragment
import xyz.ummo.agent.ui.serviceProvider.AddServiceProvider
import xyz.ummo.agent.ui.signup.SlideIntro
import xyz.ummo.agent.utilities.AgentViewModel
import xyz.ummo.agent.utilities.PrefManager
import java.util.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var drawer: DrawerLayout? = null
    private var mHandler: Handler? = null
    private var toolbar: Toolbar? = null
    private var navigationView: NavigationView? = null
    private var navigationHeaderTitle: TextView? = null
    private var navigationHeaderSubtitle: TextView? = null
    private var mAuth: FirebaseAuth? = null
    private var verificationTextView: TextView? = null
    private var prefManager: PrefManager? = null
    private val mode = Activity.MODE_PRIVATE
    private val mainActPrefs = "UMMO_AGENT_PREFERENCES"
    private val mAuthListener: AuthStateListener? = null
    private val mCurrentAgent: FirebaseUser? = null
    private val agentId: String? = null
    private var agentViewModel: AgentViewModel? = null
    private var jwt: String? = null
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        setUpNavigationView()
    }

    override fun onResume() {
        super.onResume()
        if (intent.getStringExtra("USER") != null) {
            displaySelectedScreen(R.id.nav_delegation)
        }
        Timber.e("EXTRA USERNAME->%s", intent.getStringExtra("USER"))
        Timber.e("EXTRA SERVICE-ID->%s", intent.getStringExtra("SERVICE_ID"))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        jwt = PreferenceManager.getDefaultSharedPreferences(this).getString("jwt", "")

        //Init Firebase Auth
        mAuth = FirebaseAuth.getInstance()

        SocketIO.mSocket?.on("online-agents") {
//            Timber.e()
        }

/*        String requestString = this.getIntent().getStringExtra("dispatch");
        Log.e(TAG, "delegateBottomSheet: requestString->"+requestString);*/

        //Init Sentry
        val context = this.applicationContext
        try {
            throw Exception("This is a test")
        } catch (e: Exception) {
            Sentry.captureException(e)
        }
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val status = OneSignal.getPermissionSubscriptionState()
        val oneToken = status.subscriptionStatus.pushToken
        val onePlayerId = status.subscriptionStatus.userId
        Timber.e("OneSignal Token->%s", oneToken)
        Timber.e("OneSignal PlayerId->%s", onePlayerId)

        //TODO: Migrate to AppRepository
        object : GetPublicServices() {
            override fun done(data: ByteArray, code: Number) {
                if (code == 200) {
                    PreferenceManager
                            .getDefaultSharedPreferences(this@MainActivity)
                            .edit()
                            .putString("providers", String(data))
                            .apply()
                    Timber.e("GetPublicServices: DATA ->%s", data.contentToString())
                } else {
                    Toast.makeText(this@MainActivity, "Cannot get Service Providers, some features may fail", Toast.LENGTH_LONG).show()
                }
            }
        }
        val drawable = ContextCompat.getDrawable(applicationContext, R.drawable.ic_second_add)
        toolbar?.overflowIcon = drawable
        mHandler = Handler()
        drawer = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer?.addDrawerListener(toggle)
        toggle.syncState()
        prefManager = PrefManager(this)
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel::class.java)
        val mainActPreferences = getSharedPreferences(mainActPrefs, mode)
        val agentName = Objects.requireNonNull(mainActPreferences.getString("AGENT_NAME", ""))
        val agentContact = Objects.requireNonNull(mainActPreferences.getString("AGENT_CONTACT", ""))
        val agentEmail = Objects.requireNonNull(mainActPreferences.getString("AGENT_EMAIL", ""))
        //        @NonNull String agentPid = Objects.requireNonNull(mainActPreferences.getString("AGENT_PID", ""));
        val signed_up = mainActPreferences.getBoolean("SIGNED_UP", true)
        val agentEntity = AgentEntity()
        agentEntity.contact = agentContact!!
        agentEntity.name = agentName!!
        agentEntity.email = agentEmail!!
        agentEntity.kin = ""
        agentEntity.pin = ""

        Timber.e("AGENT ENTITY->%s", agentEntity.email)
        agentViewModel!!.setAgent(agentEntity)
        Timber.e("AgentViewModel INSERTED")
        navigationView = findViewById(R.id.nav_view)
        val headerLayout = navigationView?.getHeaderView(0)
        agentViewModel!!.agentEntityLiveData.observe(this, Observer { agentEntity ->
            Timber.e("OnViewModelChange Observer->%s", agentEntity.name)
            navigationHeaderTitle = headerLayout?.findViewById(R.id.nav_header_username)
            navigationHeaderTitle?.text = agentEntity.name
            navigationHeaderSubtitle = headerLayout?.findViewById(R.id.nav_header_email)
            navigationHeaderSubtitle?.text = agentEntity.contact
        })
        navigationView?.setNavigationItemSelectedListener(this)
        verificationTextView = findViewById(R.id.verifyInfoTextView)

        mAuth = FirebaseAuth.getInstance()

        setUpNavigationView()
        if (savedInstanceState == null) {
            navItemIndex = 0
            displaySelectedScreen(R.id.nav_home)
        }
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.action_service_provider) {
            intentToAddServiceProvider()
            return true
        } else if (id == R.id.action_product) {
            intentToAddProduct()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.itemId
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    private fun setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView!!.setNavigationItemSelectedListener { menuItem ->

            // This method will trigger on item Click of navigation menu
            displaySelectedScreen(menuItem.itemId)
            true
        }
        val actionBarDrawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {
            override fun onDrawerClosed(drawerView: View) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView)
            }

            override fun onDrawerOpened(drawerView: View) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView)
            }
        }

        //Setting the actionbarToggle to drawer layout
        drawer!!.setDrawerListener(actionBarDrawerToggle)

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState()
    }

    private fun displaySelectedScreen(itemId: Int) {
        Timber.e("displaySelectedScreen")
        var selectedFragment: Fragment? = null
        when (itemId) {
            R.id.nav_home -> selectedFragment = HomeFragment()
            R.id.nav_profile -> selectedFragment = ProfileFragment()
            R.id.nav_self_delegate -> selectedFragment = SelfDelegateFragment()
            R.id.nav_delegation -> selectedFragment = DelegatedServiceFragment()
            R.id.nav_delegation_chat -> {
                val intent = Intent(this, DelegationChat::class.java)
                startActivity(intent)
            }
        }
        if (selectedFragment != null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            if (intent.extras != null) {
                val bundle = Bundle()
                bundle.putString("USER", intent.extras!!.getString("USER"))
                bundle.putString("CONTACT", intent.extras!!.getString("CONTACT"))
                bundle.putString("SERVICE_ID", intent.extras!!.getString("SERVICE_ID"))
                selectedFragment.arguments = bundle
                fragmentTransaction.replace(R.id.frame, selectedFragment).commit()
            } else {
                fragmentTransaction.replace(R.id.frame, selectedFragment)
                fragmentTransaction.commit()
            }
        }
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawerLayout.closeDrawers()
    }

    private fun intentToAddProduct() {
        val intent = Intent(this, AddProduct::class.java)
        startActivity(intent)
        finish()
    }

    private fun intentToAddServiceProvider() {
        val intent = Intent(this, AddServiceProvider::class.java)
        startActivity(intent)
        finish()
    }

    fun logout(view: View?) {
        mAuth!!.signOut()
        val progress = ProgressDialog(this@MainActivity)
        progress.setMessage("Logging out...")
        progress.show()
        object : Logout(this) {
            override fun done() {
                startActivity(Intent(this@MainActivity, SlideIntro::class.java))
            }
        }
    }

    companion object {
        private const val TAG = "MainAct"

        // index to identify current nav menu item
        var navItemIndex = 0
    }
}