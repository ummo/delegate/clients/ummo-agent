package xyz.ummo.agent.ui;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import xyz.ummo.agent.R;
import xyz.ummo.agent.data.entity.AgentEntity;
import xyz.ummo.agent.utilities.AgentViewModel;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String TAG = "AgentProfileFrag.";
    private final int mode = Activity.MODE_PRIVATE;
    private final String profileFragPrefString = "UMMO_AGENT_PREFERENCES";
    private TextView agentProfileName, agentProfileBio, agentContactInfo, agentPersonalInfo, agentServiceRecord;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private AgentViewModel agentViewModel;

    public ProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        Objects.requireNonNull(getActivity()).setTitle("Agent Profile");
        agentProfileName = view.findViewById(R.id.agent_profile_name);
        agentContactInfo = view.findViewById(R.id.agent_contact_info);
        agentPersonalInfo = view.findViewById(R.id.agent_personal_info);
        agentServiceRecord = view.findViewById(R.id.agent_service_record);

        //Using the ViewModelProviders to associate the ViewModel
        // with this UI controller. When the app first starts,
        // the ViewModelProviders will create the ViewModel.
        // When the activity is destroyed (e.g. on configuration change)
        // , the ViewModel will persist. When the activity is re-created,
        // the ViewModelProviders return the existing ViewModel
        agentViewModel = ViewModelProviders.of(this).get(AgentViewModel.class);

        agentViewModel.getAgentEntityLiveData().observe(this, new Observer<AgentEntity>() {
            @Override
            public void onChanged(AgentEntity agentEntity) {
                Log.e(TAG, "OnViewModelChange Observer->"+agentEntity.getName());
                agentProfileName.setText(agentEntity.getName());
                agentContactInfo.setText(agentEntity.getContact());
            }
        });
        SharedPreferences profileFragPreferences = Objects.requireNonNull(getActivity()).getSharedPreferences(profileFragPrefString, mode);
        String agentName = profileFragPreferences.getString("AGENT_NAME","");
        String agentContact = profileFragPreferences.getString("AGENT_CONTACT", "");
        /*agentProfileName.setText(agentName);
        agentContactInfo.setText(agentContact);*/

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        /*if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }*/
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
