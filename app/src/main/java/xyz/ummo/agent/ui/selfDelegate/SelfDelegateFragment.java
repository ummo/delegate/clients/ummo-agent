package xyz.ummo.agent.ui.selfDelegate;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import xyz.ummo.agent.R;
import xyz.ummo.agent.adapters.SelfDelegateProductsAdapter;
import xyz.ummo.agent.delegate.GetProducts;
import xyz.ummo.agent.delegate.Product;
import xyz.ummo.agent.utilities.get;

public class SelfDelegateFragment extends Fragment {

    private SelfDelegateViewModel mViewModel;
    private RecyclerView productsRecyclerView;
    private SelfDelegateProductsAdapter selfDelegateProductsAdapter;

    private static final String TAG = "SelfDelegateFragment";
    private Button selfDelegateButton;
    private ArrayList<Product> products = new ArrayList<>();
    private ProgressBar loadProductsProgressBar;

    public SelfDelegateFragment(){ }
    public static SelfDelegateFragment newInstance() {
        return new SelfDelegateFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*getActivity().runOnUiThread(() -> {
            if (products.isEmpty())
                loadProductsProgressBar.setVisibility(View.VISIBLE);
            else
                loadProductsProgressBar.setVisibility(View.GONE);
        });*/

        addProducts();
        selfDelegateProductsAdapter = new SelfDelegateProductsAdapter(getContext(), products);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.self_delegate_fragment, container, false);

        productsRecyclerView = view.findViewById(R.id.self_delegate_products_rv);
        productsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        productsRecyclerView.setItemAnimator(new DefaultItemAnimator());

        productsRecyclerView.setAdapter(selfDelegateProductsAdapter);

        loadProductsProgressBar = view.findViewById(R.id.load_products_progress_bar);
        loadProductsProgressBar.setVisibility(View.VISIBLE);

        selfDelegateButton = view.findViewById(R.id.self_delegate_button);

        Log.e(TAG, "onCreateView: PRODUCTS->"+products.size());

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(SelfDelegateViewModel.class);
        // TODO: Use the ViewModel
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void addProducts(){

        new GetProducts(Objects.requireNonNull(getActivity())){
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void done(@NotNull byte[] data, @NotNull Number code) {
                try {
                    JSONArray arr = new JSONArray(new String(data));

                    for (int i = 0; i < arr.length(); i++) {

                        JSONObject obj = arr.getJSONObject(i);
                        Log.e(TAG, "done: OBJ->"+obj.toString());

                        JSONArray processes = obj.getJSONArray("procurement_process");
                        JSONArray docs = (JSONArray) get.INSTANCE.get(obj, "requirements.documents", "docs");

                        products.add(
                                new Product(obj.getString("product_name"),
                                        /*publicServiceData.getTown(),
                                        publicServiceData.getProvince(),*/
                                        obj.getString("_id"),
                                        get.INSTANCE.get(obj, "product_description", "description").toString(),
                                        processes,
                                        get.INSTANCE.get(obj, "duration", "duration").toString(),
                                        docs,
                                        get.INSTANCE.get(obj, "requirements.procurement_cost", "cost").toString()
//                                        get.INSTANCE.get(obj, "")
                                        // TODO: 10/16/19 -> Insert `procurement_cost`
                                        // TODO: 10/16/19 -> Use ArrayLists where needed
                                ));

                        Log.e(TAG, "done: OBJECT->"+obj.toString());
                    }

                    loadProductsProgressBar.setVisibility(View.INVISIBLE);

                    selfDelegateProductsAdapter.notifyDataSetChanged();

                } catch (JSONException jse) {
                    Log.e("SelfDelegateFrag", jse.toString());
                }
            }
        };
    }
}
