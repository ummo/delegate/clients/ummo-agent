package xyz.ummo.agent.ui;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBufferResponse;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationRequest;
import  com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.model.RuntimeRemoteException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import com.google.android.libraries.places.api.Places;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.Snackbar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import timber.log.Timber;
import xyz.ummo.agent.data.entity.DelegatedServiceEntity;
import xyz.ummo.agent.delegate.AcceptDispatch;
import xyz.ummo.agent.delegate.Agent;
import xyz.ummo.agent.ui.delegatedService.DelegatedServiceFragment;
import xyz.ummo.agent.ui.delegatedService.DelegatedServiceViewModel;
import xyz.ummo.agent.adapters.PlaceAutoCompleteAdapter;
import xyz.ummo.agent.R;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment implements GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationClickListener,
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private GoogleMap mMap;
    private View mapView;
    private static final int MY_LOCATION_REQUEST_CODE = 1;
    SupportMapFragment mapFrag;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    GoogleApiClient lGoogleApiClient;
    private android.location.Location mLastLocation;
    private Marker mCurrLocationMarker;
    private int requestCode;
    private String[] permissions;
    private int[] grantResults;
    private AutoCompleteTextView searchLocationTxt;
    private PlaceAutoCompleteAdapter mAdapter;
    private NestedScrollView delegateScrollView;
    private BottomSheetBehavior bottomSheetBehavior;
    private Button acceptDelegationButton;
    private DelegatedServiceViewModel delegatedServiceViewModel;
    private DelegatedServiceEntity delegatedServiceEntity = new DelegatedServiceEntity();

    private TextView newRequestTextView, userNameTextView,
            userLocationTextView, requestedProductTextView,
            productLocationTextView, acceptingRequestTextView, loadingRequestTextView;

    private LinearLayout newRequestLayout, awaitingRequestLayout, loadingRequestLayout;

    private String productId,userId, userName, productName, productDescription,
            productCost, serviceDuration;
    private JSONArray serviceProcesses, procurementDocs;

    private Bundle bundle = new Bundle();

    private final int mode = Activity.MODE_PRIVATE;
    private final String homeFragPreferences = "UMMO_AGENT_PREFERENCES";

    private static final LatLngBounds latLngBounds = new LatLngBounds(new LatLng(-40, -168),
            new LatLng(71, 136));

    protected GeoDataClient mGeoDataClient;

    private ImageView searchButton;
    private String jwt;

    public HomeFragment() {
        // Required empty public constructor
    }

    static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        jwt = PreferenceManager.getDefaultSharedPreferences(HomeFragment.this.getContext()).getString("jwt", "");

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        if(!Places.isInitialized()){
            Places.initialize(requireContext(), "AIzaSyDBd47IkfyqjEO4lgrb59cs-4ycRulrztc");
        }

        String requestString = getActivity().getIntent().getStringExtra("dispatch");
        Timber.e("onCreate: requestString->%s", requestString);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        newRequestTextView = view.findViewById(R.id.new_request_text_view);
        userNameTextView = view.findViewById(R.id.requesting_user_name);
        userLocationTextView = view.findViewById(R.id.distance_away_text_view);
        requestedProductTextView = view.findViewById(R.id.product_name_text_view);
        productLocationTextView = view.findViewById(R.id.product_location_text_view);
        loadingRequestTextView = view.findViewById(R.id.loading_request_text_view);
        acceptingRequestTextView = view.findViewById(R.id.accepting_request_text_view);
        newRequestLayout = view.findViewById(R.id.new_request_layout);
        awaitingRequestLayout = view.findViewById(R.id.awaiting_request_layout);
        loadingRequestLayout = view.findViewById(R.id.loading_request_layout);

        requireActivity().setTitle("Ummo Agent");
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        assert mapFragment != null;
        mapView = mapFragment.getView();
        mapFragment.getMapAsync(this);

//        assign search location textview
        searchLocationTxt = view.findViewById(R.id.search_location_txt);
        //ScrollView
        delegateScrollView = view.findViewById(R.id.accept_request_sheet);
        delegateBottomSheetBehavior(view);

//        mGeoDataClient = Places.getGeoDataClient(getActivity(), null);

        searchLocationTxt.setOnItemClickListener(mAutocompleteClickListener);

//      instantiate the PlaceAutocompleteAdapter passing the context as an argument
        mAdapter = new PlaceAutoCompleteAdapter(getContext());

//      set the search location textView an adapter for auto suggesting places
        searchLocationTxt.setAdapter(mAdapter);

//        assign the search button
        searchButton = view.findViewById(R.id.search_btn);

//        add an onclick to the search button
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchLocation();
            }
        });

        return view;
    }

    private void delegateBottomSheetBehavior(View view){
        bottomSheetBehavior = BottomSheetBehavior.from(delegateScrollView);
        bottomSheetBehavior.setHideable(false);
        bottomSheetBehavior.setPeekHeight(200, true);

        String requestString = getActivity().getIntent().getStringExtra("dispatch");
        Timber.e("delegateBottomSheet: requestString->%s", requestString);
        //Activating BottomSheet once a Request is received
        if (requestString!=null){
            try {
                JSONObject requestObject = new JSONObject(requestString);
                String requestingUserName = requestObject.getJSONObject("user").getString("name");
                String requestedProduct = requestObject.getJSONObject("product").getString("product_name");
                userNameTextView.setText(requestingUserName);
                requestedProductTextView.setText(requestedProduct);
                bundle.putString("DELEGATING_USER_NAME",requestingUserName);
                Timber.e("delegateBottomSheet: bundle->%s", bundle);
            } catch (JSONException jse){
                Timber.e(jse.toString());
            }

            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    switch (newState){
                        case BottomSheetBehavior.STATE_COLLAPSED:
                            newRequestLayout.setVisibility(View.VISIBLE);
                            awaitingRequestLayout.setVisibility(View.GONE);
                            loadingRequestLayout.setVisibility(View.GONE);
                            break;
                        case BottomSheetBehavior.STATE_DRAGGING:
                        case BottomSheetBehavior.STATE_HALF_EXPANDED:
                            newRequestLayout.setVisibility(View.GONE);
                            awaitingRequestLayout.setVisibility(View.GONE);
                            loadingRequestLayout.setVisibility(View.VISIBLE);
                            break;
                        case BottomSheetBehavior.STATE_EXPANDED:
                            newRequestLayout.setVisibility(View.VISIBLE);
                            awaitingRequestLayout.setVisibility(View.GONE);
                            loadingRequestLayout.setVisibility(View.GONE);
                            //AcceptButton
                            acceptDelegationButton = view.findViewById(R.id.accept_btn);
                            acceptDelegationButton.setOnClickListener(v -> {
                                String dataString = getActivity().getIntent().getStringExtra("dispatch");
                                try {
                                    JSONObject obj = new JSONObject(dataString);

                                    Timber.e(obj.toString());
                                    Timber.e("onAcceptDispatch: dataString->%s", dataString);

                                    ArrayList<String> progress = new ArrayList<>();
                                    loadingRequestLayout.setVisibility(View.VISIBLE);
                                    loadingRequestTextView.setVisibility(View.GONE);
                                    acceptingRequestTextView.setVisibility(View.VISIBLE);
                                    newRequestLayout.setVisibility(View.GONE);

                                    userId = obj.getJSONObject("user").getString("_id");
                                    userName = obj.getJSONObject("user").getString("name");

                                    productId = obj.getJSONObject("product").getString("_id");
                                    SharedPreferences homeFragPrefs = getActivity().getSharedPreferences(homeFragPreferences, mode);
                                    homeFragPrefs.edit().putString("DELEGATED_PRODUCT_ID", productId).apply();

                                    productName = obj.getJSONObject("product").getString("product_name");
                                    productDescription = obj.getJSONObject("product").getString("product_description");
                                    procurementDocs = obj.getJSONObject("product").getJSONObject("requirements").getJSONArray("documents");
                                    productCost = obj.getJSONObject("product").getJSONObject("requirements").getString("procurement_cost");
                                    serviceDuration = obj.getJSONObject("product").getString("duration");
                                    serviceProcesses = obj.getJSONObject("product").getJSONArray("procurement_process");

                                    if (jwt != null) {
                                        Timber.e("procurementDocs->%s", procurementDocs);
                                        new AcceptDispatch(getActivity(),userId,obj.getJSONObject("product").getString("_id"), Agent.Companion.getAgentId(jwt),"E23.00"){
                                            @Override
                                            public void done(@NotNull byte[] data, int code) {
                                                Timber.e(new String(data));
                                                try {
                                                    JSONObject j = new JSONObject(new String(data));
                                                    PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("SERVICE_ID",j.getString("_id")).apply();
                                                    delegatedServiceViewModel = ViewModelProviders.of(getActivity()).get(DelegatedServiceViewModel.class);
                                                    delegatedServiceEntity.setDelegatedServiceId(j.getString("_id"));
                                                    delegatedServiceEntity.setDelegatedUserName(userName);
                                                    delegatedServiceEntity.setDelegatedServiceName(productName);
                                                    delegatedServiceEntity.setDelegatedServiceDescription(productDescription);
                                                    delegatedServiceEntity.setDelegatedServiceCost(productCost);
                                                    delegatedServiceEntity.setDelegatedServiceDuration(serviceDuration);
                                                    delegatedServiceEntity.setDelegatedServiceProgress(progress);
                                                    delegatedServiceViewModel.insertDelegatedService(delegatedServiceEntity);

                                                    Timber.e("done: %s", delegatedServiceEntity.getDelegatedServiceId());

                                                    ArrayList<String> docsList = new ArrayList<>();
                                                    if (procurementDocs!=null){
                                                        for (int i = 0; i<procurementDocs.length(); i++){
                                                            docsList.add(procurementDocs.getString(i));
                                                            delegatedServiceEntity.setDelegatedServiceDocs(docsList);
                                                            Timber.e("procurementDocs->%s", docsList);
                                                        }
                                                    }
                                                    ArrayList<String> stepsList = new ArrayList<>();
                                                    if (serviceProcesses!=null){
                                                        for (int k = 0; k<serviceProcesses.length(); k++){
                                                            stepsList.add(serviceProcesses.getString(k));
                                                            delegatedServiceEntity.setDelegatedServiceSteps(stepsList);
                                                            Timber.e("procurementSteps->%s", stepsList);
                                                        }
                                                    }

                                                }catch (JSONException e){
                                                    Timber.e("Houston, we have a Prob! %s", e.toString());
                                                }
                                                //Dismissing BottomSheet
                                                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

                                                goToDelegatedService();
                                            }
                                        };
                                    }
                                }catch (JSONException jse){
                                    Timber.e(jse.toString());
                                }
                            });
                            break;
                        case BottomSheetBehavior.STATE_HIDDEN:
                            break;
                        case BottomSheetBehavior.STATE_SETTLING:
                            break;
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                   loadingRequestLayout.setVisibility(View.VISIBLE);
                   awaitingRequestLayout.setVisibility(View.GONE);
                   newRequestLayout.setVisibility(View.GONE);
                }
            });
            //Hiding the BottomSheet, otherwise
        }
        else {
//            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            newRequestTextView.setText(getString(R.string.awaiting_new_request));
            bottomSheetBehavior.setPeekHeight(200,true);
            Timber.e("delegateBottomSheetBehav. has no EXTRA!");
            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_DRAGGING){
//                        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        newRequestLayout.setVisibility(View.GONE);
                        awaitingRequestLayout.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {

                }
            });
        }
    }

    private void goToDelegatedService(){
        Fragment delegatedServiceFragment = new DelegatedServiceFragment();
        FragmentManager fragmentManager = getFragmentManager();
        assert fragmentManager != null;
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        delegatedServiceFragment.setArguments(bundle);
        Timber.e("goToDelService: bundle->%s", bundle);
        fragmentTransaction.replace(R.id.frame, delegatedServiceFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        //if (context instanceof OnFragmentInteractionListener) {
        //  mListener = (OnFragmentInteractionListener) context;
        //} else {
        //  throw new RuntimeException(context.toString()
        //        + " must implement OnFragmentInteractionListener");
        //}
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        checkGPSNetworkEnabled();


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }

            init();
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            init();
        }

        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 100, 100);
        }
    }

    @Override
    public boolean onMyLocationButtonClick() {
        Toast.makeText(getContext(), "MyLocation button clicked", Toast.LENGTH_SHORT).show();
        // Return false so that we don't consume the event and the default behavior still occurs
        // (the camera animates to the user's current position).
        return false;
    }

    @Override
    public void onMyLocationClick(@NonNull android.location.Location location) {
        Toast.makeText(getContext(), "Current location:\n" + location, Toast.LENGTH_LONG).show();
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(requireContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(android.location.Location location) {

        Double lat = location.getLatitude();
        Double lng = location.getLongitude();

        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

        storeLatLong(lat, lng);

        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }



    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

//    private void checkLocationPermission() {
//        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
//                != PackageManager.PERMISSION_GRANTED) {
//
//            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
//                    Manifest.permission.ACCESS_FINE_LOCATION)) {
//
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//                new AlertDialog.Builder(getContext())
//                        .setTitle("Location Permission Needed")
//                        .setMessage("This app needs the Location permission, please accept to use location functionality")
//                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                //Prompt the user once explanation has been shown
//                                ActivityCompat.requestPermissions(getActivity(),
//                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                                        MY_PERMISSIONS_REQUEST_LOCATION );
//                            }
//                        })
//                        .create()
//                        .show();
//
//
//            } else {
//                // No explanation needed, we can request the permission.
//                ActivityCompat.requestPermissions(getActivity(),
//                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
//                        MY_PERMISSIONS_REQUEST_LOCATION );
//            }
//        }
//    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NotNull String[] permissions, @NotNull int[] grantResults) {
        this.requestCode = requestCode;
        this.permissions = permissions;
        this.grantResults = grantResults;
        Timber.e("Hello");
        // If request is cancelled, the result arrays are empty.
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Timber.e("here");
                // permission was granted, yay! Do the
                // location-related task you need to do.
                if (ContextCompat.checkSelfPermission(requireContext(),
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    Timber.e("check self permission");

                    if (mGoogleApiClient == null) {
                        buildGoogleApiClient();
                    }
                    mMap.setMyLocationEnabled(true);
                }

            } else {

                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Toast.makeText(getContext(), "permission denied", Toast.LENGTH_LONG).show();
            }
            //return;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    public void init(){

        mAdapter = new PlaceAutoCompleteAdapter(getContext());
        searchLocationTxt.setAdapter(mAdapter);


        searchLocationTxt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if(actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        || event.getAction() == KeyEvent.KEYCODE_ENTER){


                    searchLocation();
                }

                return false;
            }
        });
    }

    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a AutocompletePrediction from which we
             read the place ID and title.
              */
            final PlaceAutoCompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
            assert item != null;
            final String placeId = String.valueOf(item.placeId);

            searchLocation();

            /*
             Issue a request to the Places Geo Data Client to retrieve a Place object with
             additional details about the place.
              */
//            Task<PlaceBufferResponse> placeResult = mGeoDataClient.getPlaceById(placeId);
//            placeResult.addOnCompleteListener(mUpdatePlaceDetailsCallback);


            Timber.i("Called getPlaceById to get Place details for %s", placeId);
        }
    };

    /**Toast.makeText(getContext(), "Clicked: " + primaryText,
     Toast.LENGTH_SHORT).show();
     * Callback for results from a Places Geo Data Client query that shows the first place result in
     * the details view on screen.
     */
    private OnCompleteListener<PlaceBufferResponse> mUpdatePlaceDetailsCallback
            = new OnCompleteListener<PlaceBufferResponse>() {
        @Override
        public void onComplete(Task<PlaceBufferResponse> task) {
            try {
                PlaceBufferResponse places = task.getResult();

                // Get the Place object from the buffer.
                assert places != null;
                final Place place = places.get(0);

                // Format details of the place for display and show it in a TextView.

                Toast.makeText(getContext(), formatPlaceDetails(getResources(), place.getName(),
                        place.getId(), place.getAddress(), place.getPhoneNumber(),
                        place.getWebsiteUri()),
                        Toast.LENGTH_SHORT).show();

                // Display the third party attributions if set.
                final CharSequence thirdPartyAttribution = places.getAttributions();
                if (thirdPartyAttribution == null) {
                } else {

                    Toast.makeText(getContext(), Html.fromHtml(thirdPartyAttribution.toString()),
                            Toast.LENGTH_SHORT).show();
                }

                Timber.i("Place details received: %s", place.getName());

                places.release();
            } catch (RuntimeRemoteException e) {
                // Request did not complete successfully
                Timber.e(e, "Place query did not complete.");
            }
        }
    };

    private static Spanned formatPlaceDetails(Resources res, CharSequence name, String id,
                                              CharSequence address, CharSequence phoneNumber, Uri websiteUri) {
        Timber.e(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));
        return Html.fromHtml(res.getString(R.string.place_details, name, id, address, phoneNumber,
                websiteUri));

    }

    private void searchLocation(){


//      get the search string and assign it to the string address
        String address = searchLocationTxt.getText().toString();

        List<Address> addressList = null;

//      instantiate the MarkerOptions class
        MarkerOptions userMarkerOptions = new MarkerOptions();


//        check if the address is no null
        if(!TextUtils.isEmpty(address)){

//            Instantiate the geocoder class ( to change the address to real places on the world map)
            Geocoder geocoder = new Geocoder(getContext());

            try {

//                assign the addressList to a maximum of 6 results from the address passed by the user
                addressList = geocoder.getFromLocationName(address, 6);

//                check if address is not null again
                if ( address != null){

                    for (int i = 0; i <addressList.size(); i++){

//                        assign Address to the one from the addressList
                        Address userAddress = addressList.get(i);

//                        instantiate the Latitude and longitude based of the address
                        LatLng latLng = new LatLng(userAddress.getLatitude(), userAddress.getLongitude());

//                        position the maker
                        userMarkerOptions.position(latLng);

//                        set the title of the marker
                        userMarkerOptions.title(address);

//                        set the marker icon
                        userMarkerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

//                        add the marker on the map
                        mMap.addMarker(userMarkerOptions);

                        //move map camera
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

//                        animate the camera movement
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

                    }
                }else{

//                    if address is null issue a message that location is not found
                    Toast.makeText(getContext(), "Location not found", Toast.LENGTH_SHORT).show();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }else{

//            if the search location textview is empty prompt the user to input any location

            Toast.makeText(getContext(), " please write any location name", Toast.LENGTH_SHORT).show();

        }
    }

    private void checkGPSNetworkEnabled(){

//        LocationManager class provides access to system location services
        LocationManager lm = (LocationManager) requireActivity().getSystemService(Context.LOCATION_SERVICE);

        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {

//            returnS the current enabled/disabled status of the GPS _PROVIDER
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);

        } catch(Exception ex) {
            Snackbar.make(requireView().findViewById(android.R.id.content), "Exception->"+ex,
                    Snackbar.LENGTH_SHORT).show();
        }

        try {

//            return the current enabled/disabled status of the NETWORK_PROVIDER
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

        } catch(Exception ex) {
            Snackbar.make(requireView().findViewById(android.R.id.content), "Exception->"+ex,
                    Snackbar.LENGTH_SHORT).show();
        }

//        Checks if location in the system is enabled or disabled
        if(!gps_enabled && !network_enabled) {

            // notify user
            AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                    .setMessage("GPS Network not enabled")
                    .setPositiveButton("Open Location Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {

//                            opens location settings
                            requireActivity().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                        }
                    })
                    .setNegativeButton("Cancel", null).create();

            alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
                    btnPositive.setTextSize(13);

                    Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
                    btnNegative.setTextSize(13);
                }
            });

            alertDialog.show();
        }
    }

    private void storeLatLong(Double lat, Double lng){

        SharedPreferences latLngPref = requireActivity().getSharedPreferences("LatLngPref", MODE_PRIVATE);
        SharedPreferences.Editor prefEditor;
        prefEditor = latLngPref.edit();
        prefEditor.putFloat("lat", lat.floatValue());
        prefEditor.putFloat("lng", lng.floatValue());
        prefEditor.apply();


        float latPref  = latLngPref.getFloat("lat", 0);
        float lngPref = latLngPref.getFloat("lng", 0);

        Toast.makeText(getContext(), latPref + " , " +lngPref + "", Toast.LENGTH_LONG).show();
    }
}
