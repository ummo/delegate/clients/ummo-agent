package xyz.ummo.agent.ui.addProduct;

import android.content.Intent;
import android.os.Bundle;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import xyz.ummo.agent.R;

public class AddProductAttribute extends AppCompatActivity {

    EditText editText;
    String textToEdit, toolBarTitle, attribute;
    private AutoCompleteTextView textView;
    private String[] serviceProviders;
    TextView addProviderButton;
    private Map<String,String> providers = new HashMap<>();
    String[] emptyArray = new String[]{};
    private static final String TAG = "AddServiceAttrib.";
    public static final String EXTRA_NAME =
            "xyz.ummo.agent.ui.addProduct.EXTRA_NAME";
    public static final String EXTRA_DESCRIPTION =
            "xyz.ummo.agent.ui.addProduct.EXTRA_DESCRIPTION";
    public static final String EXTRA_PROVIDER =
            "xyz.ummo.agent.ui.addProduct.EXTRA_PROVIDER";
    public static final String EXTRA_DOCUMENTS =
            "xyz.ummo.agent.ui.addProduct.EXTRA_DOCUMENTS";
    public static final String EXTRA_COST =
            "xyz.ummo.agent.ui.addProduct.EXTRA_COST";
    public static final String EXTRA_DURATION =
            "xyz.ummo.agent.ui.addProduct.EXTRA_DURATION";
    public static final String EXTRA_STEPS =
            "xyz.ummo.agent.ui.addProduct.EXTRA_STEPS";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product_attribute);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel_music);

        //TODO: Replace with ServiceProviders LiveData
        getServiceProviders();

        editText = findViewById(R.id.edit_service_attribute_text);
        textView = findViewById(R.id.service_provider_autoText);

        attribute = getIntent().getStringExtra("attribute");

        textToEdit = getIntent().getStringExtra("name");

        Log.e(TAG,"Text to edit: Attribute->"+attribute);

        if(attribute.equals("provider")){
            editText.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);
          //  textView.setHint(textToEdit);

            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                    this, android.R.layout.simple_dropdown_item_1line, serviceProviders);

            final ArrayAdapter<String> notFoundArrayAdapter = new ArrayAdapter<String>(
                    this, R.layout.not_found_drop_down,  R.id.add_service_provider_button,
                    emptyArray);

            textView.setAdapter(arrayAdapter);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View arg0) {
                    textView.showDropDown();
                }
            });

        }else{
            editText.setHint(textToEdit);
        }

        toolBarTitle = getIntent().getStringExtra("toolBarTitle");
        toolbar.setTitle(toolBarTitle);

        setTitle(toolBarTitle);

    }

    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_add_attribute, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case android.R.id.home:
                Intent intent = new Intent(this, AddProduct.class);
                startActivity(intent);
                finish();
                return  true;

            case R.id.action_save:

                intentBackWithResult();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void getServiceProviders(){
        String listString = PreferenceManager
                .getDefaultSharedPreferences(this)
                .getString("providers","[]");
        try {
            JSONArray arr = new JSONArray(listString);
            serviceProviders = new String[arr.length()];
            for (int i=0; i<arr.length();i++){
                providers.put(arr.getJSONObject(i).getString("service_name"),arr.getJSONObject(i).getString("_id"));
                serviceProviders[i] = arr.getJSONObject(i).getString("service_name");
                Log.e(TAG, "getServiceProviders ->"+ Arrays.toString(serviceProviders));
            }

        }catch (JSONException jse){
            Log.e(TAG, "Providers failed to load");
            Toast.makeText(this, "Providers failed to load!",Toast.LENGTH_LONG).show();
        }
    }

    public void intentBackWithResult() {

        switch (attribute) {
            case "service name": {

                Intent intentNameData = new Intent(this, AddProduct.class);
                intentNameData.putExtra(EXTRA_NAME, editText.getText().toString());
                setResult(RESULT_OK, intentNameData);
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("service name", editText.getText().toString()).apply();
                startActivity(intentNameData);
                finish();

                break;
            }
            case "description": {

                Intent intentDescriptionData = new Intent(this, AddProduct.class);
                intentDescriptionData.putExtra(EXTRA_DESCRIPTION, editText.getText().toString());
                setResult(RESULT_OK, intentDescriptionData);
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                editText.setMaxLines(3);
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("description", editText.getText().toString()).apply();
                startActivity(intentDescriptionData);
                finish();

                break;
            }
            case "provider": {

                Intent intentProviderData = new Intent(this, AddProduct.class);
                String provider_name = textView.getText().toString();
                Log.e(TAG, "Provider->"+providers.get(provider_name));
                intentProviderData.putExtra(EXTRA_PROVIDER, editText.getText().toString());
                setResult(RESULT_OK, intentProviderData);
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("provider", providers.get(provider_name)).apply();
                startActivity(intentProviderData);
                finish();

                break;
            }
            case "documents": {

                Intent intentDocsData = new Intent(this, AddProduct.class);
                intentDocsData.putExtra(EXTRA_DOCUMENTS, editText.getText().toString());
                setResult(RESULT_OK, intentDocsData);
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("documents", editText.getText().toString()).apply();
                startActivity(intentDocsData);
                finish();

                break;
            }
            case "cost": {

                Intent intentCostData = new Intent(this, AddProduct.class);
                intentCostData.putExtra(EXTRA_COST, editText.getText().toString());
                setResult(RESULT_OK, intentCostData);
                editText.setInputType(InputType.TYPE_CLASS_NUMBER);
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("cost", editText.getText().toString()).apply();
                startActivity(intentCostData);
                finish();

                break;
            }
            case "duration": {

                Intent intentDurationData = new Intent(this, AddProduct.class);
                intentDurationData.putExtra(EXTRA_DURATION, editText.getText().toString());
                setResult(RESULT_OK, intentDurationData);
                editText.setInputType(InputType.TYPE_DATETIME_VARIATION_TIME);
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("duration", editText.getText().toString()).apply();
                startActivity(intentDurationData);
                finish();

                break;
            }
            case "steps": {

                Intent intentStepsData = new Intent(this, AddProduct.class);
                intentStepsData.putExtra(EXTRA_STEPS, editText.getText().toString());
                setResult(RESULT_OK, intentStepsData);
                PreferenceManager.getDefaultSharedPreferences(this).edit().putString("steps", editText.getText().toString()).apply();
                startActivity(intentStepsData);
                finish();

                break;
            }
        }
    }
}
