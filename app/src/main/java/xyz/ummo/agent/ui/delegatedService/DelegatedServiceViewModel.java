package xyz.ummo.agent.ui.delegatedService;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import xyz.ummo.agent.data.entity.DelegatedServiceEntity;
import xyz.ummo.agent.data.model.DelegatedService;
import xyz.ummo.agent.data.repo.AppRepository;

public class DelegatedServiceViewModel extends AndroidViewModel {

    private AppRepository appRepository;
    private LiveData<DelegatedServiceEntity> delegatedServiceEntityLiveData;
    private static final String TAG = "Del.ServiceVM";

    public DelegatedServiceViewModel(@NonNull Application application) {
        super(application);

        appRepository = new AppRepository(application);
        delegatedServiceEntityLiveData = appRepository.getDelegatedServiceEntityLiveData();
    }

    public void insertDelegatedService(DelegatedServiceEntity delegatedServiceEntity){
        appRepository.insertDelegatedService(delegatedServiceEntity);
        Log.e(TAG, "Inserting Delegated Service into VM->"+delegatedServiceEntity.getDelegatedServiceName());
    }

    public void updateDelegatedService(DelegatedServiceEntity delegatedServiceEntity){
        appRepository.updateDelegatedService(delegatedServiceEntity);
    }

    public void deleteAllDelegatedServices(){
        appRepository.deleteAllDelegatedServices();
    }

    public LiveData<DelegatedServiceEntity> getDelegatedServiceEntityLiveData(){
        return delegatedServiceEntityLiveData;
    }

    public LiveData<DelegatedServiceEntity> getDelegatedServiceLiveDataById(String delegatedServiceId){
        return appRepository.getDelegatedServiceById(delegatedServiceId);
    }
}
