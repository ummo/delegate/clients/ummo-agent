package xyz.ummo.agent.ui.serviceProvider

import android.Manifest
import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.appbar.AppBarLayout.OnOffsetChangedListener
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber
import xyz.ummo.agent.R
import xyz.ummo.agent.data.entity.ServiceProviderEntity
import xyz.ummo.agent.databinding.ActivityAddServiceProviderBinding
import xyz.ummo.agent.databinding.ContentAddServiceProviderBinding
import xyz.ummo.agent.delegate.CreateServiceProvider
import xyz.ummo.agent.ui.MainActivity
import java.util.*

class AddServiceProvider : AppCompatActivity() {
    private lateinit var viewBinding: ActivityAddServiceProviderBinding
    private lateinit var contentAddServiceProviderBinding: ContentAddServiceProviderBinding
    private var mCollapsingToolbarLayout: CollapsingToolbarLayout? = null
    private var toolbar: Toolbar? = null
    private var menu: Menu? = null
    private var providerNameEdt: EditText? = null
    private var provinceEdt: EditText? = null
    private var municipalityEdt: EditText? = null
    private var townEdt: EditText? = null
    private var serviceProviderViewModel: ServiceProviderViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        Timber.e("HOME!!!")
        Log.e("AddServiceProvider", "HOME!")

        /** Binding the view to `viewBinding` then setting up the UI **/
        viewBinding = ActivityAddServiceProviderBinding.inflate(layoutInflater)
        val view = viewBinding.root
        setContentView(view)

        toolbar = viewBinding.toolbar
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        /** Binding the view to `contentAddServiceProviderBinding` then setting up the UI **/
        contentAddServiceProviderBinding = ContentAddServiceProviderBinding.inflate(layoutInflater)
        /*providerNameEdt = contentAddServiceProviderBinding.providerNameEditText
        provinceEdt = contentAddServiceProviderBinding.provinceEditText
        municipalityEdt = contentAddServiceProviderBinding.municipalityEditText
        townEdt = contentAddServiceProviderBinding.townEditText*/
        mCollapsingToolbarLayout = viewBinding.collapsingToolbarLayout

        //Trying out edit text fields with ID refs
        providerNameEdt = findViewById(R.id.provider_name_edit_text)
        provinceEdt = findViewById(R.id.province_edit_text)
        municipalityEdt = findViewById(R.id.municipality_edit_text)
        townEdt = findViewById(R.id.town_edit_text)

        /** Setting up the AppBar **/
        setupAppBar()

        mCollapsingToolbarLayout!!.title = " "
        mCollapsingToolbarLayout!!.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        mCollapsingToolbarLayout!!.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        handlePermission()
        setTextViews()
    }

    private fun setupAppBar() {
        val appBar = viewBinding.appBar
        appBar.addOnOffsetChangedListener(OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (mCollapsingToolbarLayout!!.height + verticalOffset < 2 * ViewCompat.getMinimumHeight(mCollapsingToolbarLayout!!)) {
                Objects.requireNonNull(toolbar!!.navigationIcon)!!.setColorFilter(resources.getColor(R.color.black), PorterDuff.Mode.SRC_ATOP)
            } else {
                Objects.requireNonNull(toolbar!!.navigationIcon)?.setColorFilter(resources.getColor(R.color.UmmoPurple), PorterDuff.Mode.SRC_ATOP)
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_add, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val mProviderName: String = providerNameEdt!!.text.toString()
        val mProviderProvince: String = provinceEdt!!.text.toString()
        val mProviderMunicipality: String = municipalityEdt!!.text.toString()
        val mProviderTown: String = townEdt!!.text.toString().trim { it <= ' ' }
        var greenLight = false

        return when (item.itemId) {

            android.R.id.home -> {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                Timber.e("GOING HOME")

                finish()
                true
            }

            R.id.action_save -> {

                val serviceProviderEntity = ServiceProviderEntity()
                serviceProviderViewModel = ViewModelProviders.of(this).get(ServiceProviderViewModel::class.java)

                /** Checking Service Provider integrity **/
                when {
                    mProviderName.isEmpty() -> {
                        providerNameEdt!!.error = "Service Provider Name expected!"
                    }
                    mProviderName.length < 3 -> {
                        providerNameEdt!!.error = "Please enter a longer name..."
                    }
                    else -> {
                        serviceProviderEntity.serviceProviderName = mProviderName
                        greenLight = true
                    }
                }

                /** Checking Province integrity **/
                when {
                    mProviderProvince.isEmpty() -> {
                        provinceEdt!!.error = "A Province is expected!"
                    }
                    mProviderProvince.length < 3 -> {
                        provinceEdt!!.error = "Please enter a real province..."
                    }
                    else -> {
                        serviceProviderEntity.serviceProviderProvince = mProviderProvince
                        greenLight = true
                    }
                }

                /** Checking Municipality integrity **/
                when {
                    mProviderMunicipality.isEmpty() -> {
                        municipalityEdt!!.error = "A Municipality is expected!"
                    }
                    mProviderMunicipality.length < 3 -> {
                        municipalityEdt!!.error = "Please enter a real municipality..."
                    }
                    else -> {
                        serviceProviderEntity.serviceProviderMunicipality = mProviderMunicipality
                        greenLight = true
                    }
                }

                /** Checking Town integrity **/
                when {
                    mProviderTown.isEmpty() -> {
                        townEdt!!.error = "A Town is expected"
                    }
                    mProviderTown.length < 3 -> {
                        townEdt!!.error = "Please enter a real town..."
                    }
                    else -> {
                        serviceProviderEntity.serviceProviderTown = mProviderTown
                        greenLight = true
                    }
                }

                /** Checking if every field is ready to be saved
                 * `greenLight` turns "green" only after every field passes **/
                if (greenLight) {
                    val progressDialog = ProgressDialog(this)
                    progressDialog.setTitle("Saving Service Provider...")
                    progressDialog.show()

                    //Inserting ServiceProviderEntity into ServiceProviderViewModel
                    serviceProviderViewModel!!.setServiceProviderEntityLiveData(serviceProviderEntity)
                    Timber.e("onSave: ServiceProvider ViewModel  ${serviceProviderViewModel!!.serviceProviderEntityLiveData}")

                    //TODO: to be moved to appRepo for "single-source-of-truth"
                    object : CreateServiceProvider(mProviderName, mProviderProvince, mProviderMunicipality, mProviderTown) {
                        override fun done(data: ByteArray, code: Number) {
                            intentBack()
                            progressDialog.dismiss()
                        }
                    }
                }
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun handlePermission() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //ask for permission
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                    SELECT_PICTURE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == SELECT_PICTURE) {
            for (i in permissions.indices) {
                val permission = permissions[i]
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    val showRationale = ActivityCompat.shouldShowRequestPermissionRationale(this, permission)
                    if (showRationale) {
                        //  TODO:Show your own message here
                        Snackbar.make(findViewById(android.R.id.content), "Please turn on your location....",
                                Snackbar.LENGTH_SHORT).show()
                    } else {
                        showSettingsAlert()
                    }
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    /* Choose an image from Gallery */
    fun openImageChooser(view: View?) {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Thread(Runnable {
            if (resultCode == Activity.RESULT_OK) {
                if (requestCode == SELECT_PICTURE) {
                    // Get the url from data
                    val selectedImageUri = data!!.data
                    if (null != selectedImageUri) {
                        // Get the path from the Uri
                        val path = getPathFromURI(selectedImageUri)
                        Timber.e("Image Path \$path ")
                        // Set the image in ImageView
                        findViewById<View>(R.id.service_image).post { (findViewById<View>(R.id.service_image) as ImageView).setImageURI(selectedImageUri) }
                    }
                }
            }
        }).start()
    }

    /* Get the real path from the URI */
    private fun getPathFromURI(contentUri: Uri?): String? {
        var res: String? = null
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        val cursor = contentResolver.query(contentUri!!, proj, null, null, null)!!
        if (cursor.moveToFirst()) {
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            res = cursor.getString(column_index)
        }
        cursor.close()
        return res
    }

    private fun showSettingsAlert() {
        val alertDialog = AlertDialog.Builder(this).create()
        alertDialog.setTitle("Alert")
        alertDialog.setMessage("App needs to access the Camera.")
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "DONT ALLOW"
        ) { dialog, which ->
            dialog.dismiss()
            //finish();
        }
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "SETTINGS"
        ) { dialog, which ->
            dialog.dismiss()
            openAppSettings(this@AddServiceProvider)
        }
        alertDialog.show()
    }

    private fun setTextViews() {
        val providerName: String
        val province: String
        val municipality: String
        val town: String
        val sp = PreferenceManager.getDefaultSharedPreferences(this)
        providerName = sp.getString("edit_provider_name", "").toString()
        //getIntent().getStringExtra("provider name");
        province = sp.getString("province", "").toString()
        municipality = sp.getString("municipality", "").toString()
        town = sp.getString("town", "").toString()

        if (providerName != "") {
            providerNameEdt!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            providerNameEdt!!.setText(providerName)
            providerNameEdt!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_pencil_black_tool_interface_symbol, 0)
        }

        if (province != "") {
            provinceEdt!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            provinceEdt!!.setText(province)
            provinceEdt!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_pencil_black_tool_interface_symbol, 0)
        }

        if (municipality != "") {
            municipalityEdt!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            municipalityEdt!!.setText(municipality)
            municipalityEdt!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_pencil_black_tool_interface_symbol, 0)
        }

        if (town != "") {
            townEdt!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            townEdt!!.setText(town)
            townEdt!!.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_pencil_black_tool_interface_symbol, 0)
        }
    }

    override fun onDestroy() {
        val sp = PreferenceManager.getDefaultSharedPreferences(this)
        sp.edit()
                .remove("edit_provider_name")
                .remove("province")
                .remove("municipality")
                .remove("town").apply()
        super.onDestroy()
    }

    /*fun intentToServiceProviderHelper(textToEdit: String?, toolBarTitle: String?, attribute: String?) {
        val intent = Intent(this, AddServiceProviderAttribute::class.java)
        intent.putExtra("name", textToEdit)
        intent.putExtra("toolBarTitle", toolBarTitle)
        intent.putExtra("attribute", attribute)
        startActivity(intent)
    }*/

    fun intentBack() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    companion object {
        private const val SELECT_PICTURE = 100
        fun openAppSettings(context: Activity?) {
            if (context == null) {
                return
            }
            val i = Intent()
            i.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            i.addCategory(Intent.CATEGORY_DEFAULT)
            i.data = Uri.parse("package:" + context.packageName)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
            i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
            context.startActivity(i)

            //
        }
    }
}