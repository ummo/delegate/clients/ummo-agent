package xyz.ummo.agent.ui.serviceProvider;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import xyz.ummo.agent.data.entity.ServiceProviderEntity;
import xyz.ummo.agent.data.model.ServiceProvider;
import xyz.ummo.agent.data.repo.AppRepository;

public class ServiceProviderViewModel extends AndroidViewModel {
    private AppRepository appRepository;
    private LiveData<ServiceProviderEntity> serviceProviderEntityLiveData;

    public ServiceProviderViewModel(Application application){
        super(application);

        appRepository = new AppRepository(application);
        serviceProviderEntityLiveData = appRepository.getServiceProviderEntityLiveData();
    }

    public LiveData<ServiceProviderEntity> getServiceProviderEntityLiveData(){
        Log.e("ServiceProviderVM", "Service-Provider from ViewModel->"+ serviceProviderEntityLiveData);
        return serviceProviderEntityLiveData;
    }

    public void setServiceProviderEntityLiveData(ServiceProviderEntity serviceProviderEntity){
        appRepository.insertServiceProvider(serviceProviderEntity);
        Log.e("Service ProvidreVM", "INSERTED->"+serviceProviderEntity);
    }
}
