package xyz.ummo.agent.ui;

import android.content.Intent;
import android.os.Bundle;

import com.github.nkzawa.emitter.Emitter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import timber.log.Timber;
import xyz.ummo.agent.ChatBubble;
import xyz.ummo.agent.R;
import xyz.ummo.agent.delegate.GetService;
import xyz.ummo.agent.delegate.SocketIO;
import xyz.ummo.agent.delegate.SendChatMessage;
import xyz.ummo.agent.adapters.MessageAdapter;

public class DelegationChat extends AppCompatActivity {

    private ListView listView;
    private View btnSend;
    private EditText editText;
    private TextView timeStamp;
    boolean myMessage = true;
    private List<ChatBubble> ChatBubbles;
    private ArrayAdapter<ChatBubble> adapter;
    private String service_id, chatServiceId;
    private Date currentTime = Calendar.getInstance().getTime();
    private static JSONArray chatArray = new JSONArray();
    private static final String TAG = "DelegationChat.java";

    @Override
    protected void onDestroy() {
        SocketIO.INSTANCE.getMSocket().off("message");
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        SocketIO.INSTANCE.getMSocket().off("message");
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delegation_chat);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setTitle("Delegation Chat");
        service_id = PreferenceManager.getDefaultSharedPreferences(this).getString("SERVICE_ID","");

        Intent intent = getIntent();
        chatServiceId = intent.getStringExtra("service_id");
        Timber.e("onCreate: CHAT Service ID->%s", chatServiceId);
        ChatBubbles = new ArrayList<>();

        //Implement a loader
        if (service_id != null) {
            new GetService(service_id){
                @Override
                public void done(@NotNull byte[] data, @NotNull Number code) {
                    try {
                        JSONObject obj = new JSONObject(new  String(data));
                        JSONArray chatArr = obj.getJSONArray("chat");
                        for (int i = 0;i<chatArr.length(); i++){
                            JSONObject message = chatArr.getJSONObject(i);
                            String timestring = message.getString("timestamp");
                            String[] times = timestring.split(":");
                            Timber.e("Times->%s", times[0]);
                            Timber.e("GetService: chatArray->%s", chatArray);
                            ChatBubble chatBubble = new ChatBubble(message.getString("message"), currentTime.toString(), message.getString("from").equals("agent"));
                            ChatBubbles.add(chatBubble);

                        }
                        adapter.notifyDataSetChanged();

                    }catch (JSONException e){
                        Timber.e("onCreate: JSE-1->%s", e.toString());
                    }

                }
            };
        }

        /*if (chatServiceId != null){
            Log.e(TAG, "onCreate: chatServiceID->"+chatServiceId);
            new GetService(chatServiceId){
                @Override
                public void done(@NotNull byte[] data, @NotNull Number code) {
                    try {
                        JSONObject obj = new JSONObject(new  String(data));
                        JSONArray chatArr = obj.getJSONArray("chat");
                        for (int i = 0;i<chatArr.length(); i++){
                            JSONObject message = chatArr.getJSONObject(i);
                            String timestring = message.getString("timestamp");
                            String[] times = timestring.split(":");
                            Log.e(TAG, "times->"+times[0]);
                            GregorianCalendar cal = new GregorianCalendar();

                            ChatBubble chatBubble = new ChatBubble(message.getString("message"), currentTime.toString(), message.getString("from").equals("agent"));
                            ChatBubbles.add(chatBubble);

                        }
                        adapter.notifyDataSetChanged();

                    }catch (JSONException e){
                        Log.e(TAG,"onCreate: JSE-2->"+e.toString());
                    }
                }
            };
        }*/

        listView = findViewById(R.id.list_msg);
        btnSend = findViewById(R.id.send_btn);
        editText = findViewById(R.id.message);
        timeStamp = findViewById(R.id.message_time_stamp);

        //set ListView adapter first
        adapter = new MessageAdapter(this, R.layout.left_chat_bubble, ChatBubbles);
        listView.setAdapter(adapter);

        SocketIO.INSTANCE.getMSocket().on("message", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Timber.e("onMessage Sent->%s", args[0].toString());
                try {
                    JSONObject message = new JSONObject(args[0].toString());
                    ChatBubble chatBubble = new ChatBubble(message.getString("message"), currentTime.toString(), message.getString("from").equals("agent"));

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ChatBubbles.add(chatBubble);
                            Timber.e("onMessageSent: chatArray->%s", chatArray);
                            adapter.notifyDataSetChanged();
                        }
                    });

                }catch (JSONException e){
                    Timber.e("onMessage: JSE->%s", e.toString());
                }

            }
        });

        sendMessage();
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Intent upIntent = new Intent(this, DelegatedServiceFragment.class);
                upIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(upIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }*/

    private void sendMessage(){
        //event for button SEND
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (editText.getText().toString().trim().equals("")) {
                    Toast.makeText(DelegationChat.this, "Please input some text...", Toast.LENGTH_SHORT).show();
                } else {
                    new SendChatMessage(editText.getText().toString(),service_id){
                        @Override
                        public void done(@NotNull byte[] data, @NotNull Number code) {
                            Timber.e("sendMessage: service->%s", new String(data));
                            try {
                                JSONObject service = new JSONObject(new String(data));

                                chatArray = service.getJSONArray("chat");

                                for (int i = 0; i < chatArray.length(); i++){
                                    /*JSONObject chat = chatArray.getJSONObject(i);
                                    JSONArray dateTimeStamp = chatArray.getJSONObject(i).getString("timestamp");*/

                                    Timber.e("sendMessage: dateTimeStamp at (" + i + ") ->" + chatArray.getString(i));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Timber.e("sendMessage: JSE->%s", e.toString());
                            }
//                            String dateTimeStamp = new String(data);
                        }
                    };
                    //add message to list
                    editText.setText("");
                }
            }
        });
    }
}
