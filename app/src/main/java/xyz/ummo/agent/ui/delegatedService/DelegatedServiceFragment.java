package xyz.ummo.agent.ui.delegatedService;

import androidx.core.widget.NestedScrollView;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import timber.log.Timber;
import xyz.ummo.agent.data.entity.DelegatedServiceEntity;
import xyz.ummo.agent.data.repo.AppRepository;
import xyz.ummo.agent.ui.DelegationChat;
import xyz.ummo.agent.R;
import xyz.ummo.agent.ui.PaymentCollection;

public class DelegatedServiceFragment extends Fragment {

    private DelegatedServiceViewModel delegatedServiceViewModel;
    private DelegatedServiceEntity delegatedServiceEntity;
    private TextView serviceNameTextView,
            serviceDescriptionTextView,
            serviceDocsTextView,
            serviceCostTextView,
            serviceDurationTextView, expandCollapseTextView;

    private ProgressBar serviceProgressBar;
    private ImageView openChatBtn, collapseImageView, expandImageView;
    private NestedScrollView delegatedServiceScrollView;
    private CheckBox serviceStepsCheckBox;
    private final int mode = Activity.MODE_PRIVATE;
    private final String delegatedServicePrefs = "UMMO_AGENT_PREFERENCES";
    private static final String TAG = "DelegatedServiceFrag.";
    private TextView delegatedUserName;
    private Bundle bundle = new Bundle();
    private String delegatingUserName;
    private LinearLayout serviceStepsLayout, serviceDocsLayout;
    private RelativeLayout noDelegationLayout, delegationLayout, collapsibleLayout, actionLayout;
    private int serviceProgressStatus = 0;
    private Handler handler = new Handler();
    private int progressPercent;

    public static DelegatedServiceFragment newInstance() {
        return new DelegatedServiceFragment();
    }

    public DelegatedServiceFragment(){}

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bundle = getArguments();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.delegated_service_fragment, container, false);
        //Declaring ImageViews
        collapseImageView = view.findViewById(R.id.collapse_image_view);
        expandImageView = view.findViewById(R.id.expand_image_view);
        //Declaring TextViews
        serviceNameTextView = view.findViewById(R.id.delegated_service_header_name);
        serviceDescriptionTextView = view.findViewById(R.id.description_text_view);
//        serviceDocsTextView = view.findViewById(R.id.service_docs_text_view);
        serviceCostTextView = view.findViewById(R.id.service_cost_text_view);
        serviceDurationTextView = view.findViewById(R.id.service_duration_text_view);
        delegatedUserName = view.findViewById(R.id.delegated_user_name_text_view);
        serviceProgressBar = view.findViewById(R.id.service_progress_bar);
        expandCollapseTextView = view.findViewById(R.id.action_text_view);
        //Declaring Layout
        noDelegationLayout = view.findViewById(R.id.no_delegation_layout);
        delegationLayout = view.findViewById(R.id.delegation_layout);
        collapsibleLayout = view.findViewById(R.id.delegated_service_collapsible_layout);
        actionLayout = view.findViewById(R.id.action_layout);
//        serviceStepsCheckBox = view.findViewById(R.id.service_steps_checkbox);
        openChatBtn = view.findViewById(R.id.open_chat_button);
//        serviceStepsCheckBox = view.findViewById(R.id.process_check_box_1);
        //Declaring ScrollView
        delegatedServiceScrollView = view.findViewById(R.id.delegated_service_scrollview);

        serviceStepsLayout = view.findViewById(R.id.service_steps_layout);
        serviceDocsLayout = view.findViewById(R.id.service_docs_linear_layout);

        requireActivity().setTitle("Delegated Service");
        fillDelegatedServiceFields();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        delegatedServiceViewModel = ViewModelProviders.of(this).get(DelegatedServiceViewModel.class);

        // TODO: Use the ViewModel
        /*delegatedServiceViewModel.getDelegatedServiceEntityLiveData().observe(this, delegatedServiceEntity -> {
            Log.e(TAG, "VM LiveData Observer->"+delegatedServiceEntity.getDelegatedServiceName());
            serviceNameTextView.setText(delegatedServiceEntity.getDelegatedServiceName());
            serviceDescriptionTextView.setText(delegatedServiceEntity.getDelegatedServiceDescription());
            serviceDocsTextView.setText(delegatedServiceEntity.getDelegatedServiceDocs());
            serviceCostTextView.setText(delegatedServiceEntity.getDelegatedServiceCost());
            serviceDurationTextView.setText(delegatedServiceEntity.getDelegatedServiceDuration());
        });
        String productId = "5d0f6f483875b100175c099e";
        delegatedServiceViewModel.getDelegatedServiceLiveDataById(productId);*/
    }

    private void fillDelegatedServiceFields(){
        SharedPreferences delegatedServicePreferences = requireActivity().getSharedPreferences(delegatedServicePrefs, mode);
        String _productId = delegatedServicePreferences.getString("DELEGATED_PRODUCT_ID", "");

        String _serviceId = PreferenceManager.getDefaultSharedPreferences(getContext()).getString("SERVICE_ID", "");

        Timber.e("Service ID->%s", _serviceId);

        if (_serviceId != null && !_serviceId.isEmpty()) {
            //Displaying Delegation Layout && hiding default
            noDelegationLayout.setVisibility(View.GONE);
            delegationLayout.setVisibility(View.VISIBLE);
            Timber.e("fillDelegatedServiceFields: Just outside the view model");
            delegatedServiceViewModel = ViewModelProviders.of(this).get(DelegatedServiceViewModel.class);
            delegatedServiceViewModel.getDelegatedServiceLiveDataById(_serviceId).observe(getViewLifecycleOwner(), delegatedServiceEntity -> {
//                Log.e("Delegated Service", delegatedServiceEntity.getDelegatedServiceProgress().toString());
                this.delegatedServiceEntity = delegatedServiceEntity;
                delegatedUserName.setText(delegatedServiceEntity.getDelegatedUserName());
                Timber.e("fillDelegatedServiceFields: USER-NAME->%s", delegatedServiceEntity.getDelegatedUserName());
                serviceNameTextView.setText(delegatedServiceEntity.getDelegatedServiceName());
                serviceDescriptionTextView.setText(delegatedServiceEntity.getDelegatedServiceDescription());

                serviceDocsLayout.removeAllViews();
                for (int i = 0; i < delegatedServiceEntity.getDelegatedServiceDocs().size(); i++) {
                    Timber.e("size is->%s", delegatedServiceEntity.getDelegatedServiceDocs().size());
                    serviceDocsTextView = new TextView(getContext());
                    serviceDocsTextView.setId(i);

                    if (delegatedServiceEntity.getDelegatedServiceDocs().size() == 2) {
                        if (i == 1) {
                            serviceDocsTextView.setText(" & " + delegatedServiceEntity.getDelegatedServiceDocs().get(i));
                        } else
                            serviceDocsTextView.setText(delegatedServiceEntity.getDelegatedServiceDocs().get(i));
                    } else
                        serviceDocsTextView.setText(delegatedServiceEntity.getDelegatedServiceDocs().get(i));

                    serviceDocsTextView.setTextSize(14);
                    serviceDocsLayout.addView(serviceDocsTextView);
                }

                serviceCostTextView.setText(delegatedServiceEntity.getDelegatedServiceCost());
                serviceDurationTextView.setText(delegatedServiceEntity.getDelegatedServiceDuration());
                serviceStepsLayout.removeAllViews();
                assert delegatedServiceEntity.getDelegatedServiceSteps() != null;
                for (int j = 0; j < delegatedServiceEntity.getDelegatedServiceSteps().size(); j++) {
                    List steps = delegatedServiceEntity.getDelegatedServiceSteps();
                    List progress = this.delegatedServiceEntity.getDelegatedServiceProgress();
                    serviceStepsCheckBox = new CheckBox(getContext());
                    serviceStepsCheckBox.setId(j);
                    serviceStepsCheckBox.setText(delegatedServiceEntity.getDelegatedServiceSteps().get(j));
                    serviceStepsCheckBox.setTextSize(14);
                    serviceStepsCheckBox.setOnClickListener(getOnCheckOffStep(serviceStepsCheckBox));
                    serviceStepsCheckBox.setHighlightColor(getResources().getColor(R.color.ummo_1));
                    assert progress != null;
                    serviceStepsCheckBox.setChecked(progress.contains(steps.get(j)));
                    serviceStepsLayout.addView(serviceStepsCheckBox);
                    Timber.e("fillDelegatedServiceFields: %s", ((progress.size() * 100) / steps.size()));
                    serviceProgressBar.setProgress((progress.size()*100)/steps.size());

                    progressPercent = ((progress.size()*100)/steps.size());
                }

                // TODO: 12/19/19-> if progress is 100%, display confirmation dialog, then launch payment screen

                if (progressPercent == 100){
                    AlertDialog alertDialog = new AlertDialog.Builder(getContext())
                            .setTitle("Service Complete")
                            .setMessage("Confirm that you have completed the delegated service...")
                            .setPositiveButton("Confirm", (paramDialogInterface, paramInt) -> {
                                Intent collectionIntent = new Intent(getContext(), PaymentCollection.class);
                                collectionIntent.putExtra("SERVICE_ID", _serviceId);
                                startActivity(collectionIntent);
                            })
                            .setNegativeButton("Cancel", null).create();

                        /*alertDialog.setOnShowListener(dialog -> {
                            Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
                            btnPositive.setTextSize(13);

                            Button btnNegative = alertDialog.getButton(Dialog.BUTTON_NEGATIVE);
                            btnNegative.setTextSize(13);
                        });*/

                    alertDialog.show();
                }

                if (getArguments() != null) {
                    delegatingUserName = getArguments().getString("DELEGATING_USER_NAME");
                    delegatedUserName.setText(delegatingUserName);
                }

                openChatBtn.setOnClickListener(v -> {
                    Intent intent = new Intent(getActivity(), DelegationChat.class);
                    intent.putExtra("service_id", _serviceId);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    Log.e("Service", _serviceId);
                    startActivity(intent);
                });

                actionLayout.setOnClickListener(v -> {
                    Log.e(TAG, "actionLayout->" + expandCollapseTextView.getText().toString());

                    if (expandCollapseTextView.getText().equals("EXPAND")) {
                        collapsibleLayout.setVisibility(View.VISIBLE);
                        expandCollapseTextView.setText("COLLAPSE");
                        collapseImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_stat_expand_more));
                        delegatedServiceScrollView.scrollTo(0, delegatedServiceScrollView.getBottom()); //TODO:conclude this animation

                    } else if (expandCollapseTextView.getText().equals("COLLAPSE")) {
                        collapsibleLayout.setVisibility(View.GONE);
                        expandCollapseTextView.setText("EXPAND");
                        collapseImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_stat_expand_less));
                        delegatedServiceScrollView.scrollTo(0, delegatedServiceScrollView.getTop());
                    }
                });

                expandCollapseTextView.setOnClickListener(v -> {
                    Log.e(TAG, "actionLayout->" + expandCollapseTextView.getText().toString());

                    if (expandCollapseTextView.getText().equals("EXPAND")) {
                        collapsibleLayout.setVisibility(View.VISIBLE);
                        expandCollapseTextView.setText("COLLAPSE");
                        collapseImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_stat_expand_more));
                        delegatedServiceScrollView.scrollTo(0, delegatedServiceScrollView.getBottom()); //TODO:conclude this animation
                    } else if (expandCollapseTextView.getText().equals("COLLAPSE")) {
                        collapsibleLayout.setVisibility(View.GONE);
                        expandCollapseTextView.setText("EXPAND");
                        collapseImageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_stat_expand_less));
                        delegatedServiceScrollView.scrollTo(0, delegatedServiceScrollView.getTop());
                    }
                });
            });
        } else {
            //Hiding Delegation Layout && showing default
            noDelegationLayout.setVisibility(View.VISIBLE);
            delegationLayout.setVisibility(View.GONE);
        }
    }

    private View.OnClickListener getOnCheckOffStep(final CheckBox checkBox) {
        SharedPreferences delegatedServicePreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String _serviceId = delegatedServicePreferences.getString("SERVICE_ID", "");

        return v -> {
            ArrayList<String> steps = delegatedServiceEntity.getDelegatedServiceProgress();
            if(checkBox.isChecked()){
                steps.add(checkBox.getText().toString());
            }else {
                for(int i = 0; i< steps.size(); i++){
                    if(steps.get(i).equals(checkBox.getText().toString())){
                        steps.remove(i);
                    }
                }
            }

            delegatedServiceEntity.setDelegatedServiceProgress(steps);

            new AppRepository(getActivity().getApplication()).updateDelegatedService(delegatedServiceEntity);
        };
    }

    ArrayList toArrayList(JSONArray arr){
        ArrayList a = new ArrayList();
        try{
            for(int i = 0; i<arr.length();i++){
                a.add(arr.getString(i));
            }
        }catch (JSONException e){
            Log.e(TAG,e.toString());
        }
        return a;
    }
}
