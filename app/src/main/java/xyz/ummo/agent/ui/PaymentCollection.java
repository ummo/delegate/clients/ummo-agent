package xyz.ummo.agent.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import xyz.ummo.agent.R;
import xyz.ummo.agent.data.entity.DelegatedServiceEntity;
import xyz.ummo.agent.ui.delegatedService.DelegatedServiceViewModel;

public class PaymentCollection extends AppCompatActivity {

    private DelegatedServiceViewModel delegatedServiceViewModel;
    private DelegatedServiceEntity delegatedServiceEntity;

    private TextView userNameTextView, serviceFee, serviceName;
    private Button confirmPaymentButton;
    private static final String TAG = "PaymentCollection";
    private String serviceId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_collection);

        userNameTextView = findViewById(R.id.user_name);
        serviceFee = findViewById(R.id.service_price);
        serviceName = findViewById(R.id.service_name);
        confirmPaymentButton = findViewById(R.id.confirm_payment_btn);

        delegatedServiceViewModel = ViewModelProviders.of(this).get(DelegatedServiceViewModel.class);
        Log.e(TAG, "onCreate: SERVICE_ID->"+getIntent().getExtras().getString("SERVICE_ID"));

        serviceId =getIntent().getExtras().getString("SERVICE_ID");
        delegatedServiceViewModel.getDelegatedServiceLiveDataById(serviceId).observe(this, delegatedServiceEntity1 -> {
            serviceName.setText(delegatedServiceEntity1.getDelegatedServiceName());
            serviceFee.setText(delegatedServiceEntity1.getDelegatedServiceCost());
            userNameTextView.setText(delegatedServiceEntity1.getDelegatedUserName());
        });
    }
}
