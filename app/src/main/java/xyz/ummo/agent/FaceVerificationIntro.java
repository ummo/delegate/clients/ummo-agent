package xyz.ummo.agent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FaceVerificationIntro extends AppCompatActivity {

    Button proceed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_face_verification_intro);
        setTitle("Face Verification");

        proceed =  findViewById(R.id.proceed_btn);

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intentToFaceVerif();            }
        });
    }

    private void intentToFaceVerif(){


        Intent i = new Intent(this, FaceVerification.class);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);

    }
}
