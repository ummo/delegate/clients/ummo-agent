package xyz.ummo.agent.adapters;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.hbb20.CountryCodePicker;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import xyz.ummo.agent.R;
import xyz.ummo.agent.data.entity.DelegatedServiceEntity;
import xyz.ummo.agent.data.entity.ProductEntity;
import xyz.ummo.agent.delegate.Product;
import xyz.ummo.agent.delegate.SelfDelegate;
import xyz.ummo.agent.ui.MainActivity;
import xyz.ummo.agent.ui.delegatedService.DelegatedServiceViewModel;

public class SelfDelegateProductsAdapter extends RecyclerView.Adapter<SelfDelegateProductsAdapter.MyViewHolder> {

    private List<Product> productList;
    private static final String TAG = "SelfDelProductAdapter";
//    private DetailedProductViewModel detailedProductViewModel;
    private ProductEntity productEntity = new ProductEntity();
    private Context context;
    private DelegatedServiceViewModel delegatedServiceViewModel;
    private DelegatedServiceEntity delegatedServiceEntity = new DelegatedServiceEntity();
    private JSONArray serviceProcesses, procurementDocs;

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView providerName, providerLocation, serviceProvider;
        ImageView selfDelegateButton;

        RelativeLayout bg, productRL;

        MyViewHolder(View view, RelativeLayout bg) {
            super(view);
            this.bg = bg;
            productRL = view.findViewById(R.id.productRL);
            providerName = view.findViewById(R.id.product_name);
            providerLocation = view.findViewById(R.id.product_location);
            serviceProvider = view.findViewById(R.id.service_provider);
            selfDelegateButton = view.findViewById(R.id.self_delegate_button);
        }
    }

    public SelfDelegateProductsAdapter(Context context, List<Product> productList) {
        this.context = context;
        this.productList = productList;
    }

    @NonNull
    @Override
    public SelfDelegateProductsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.self_delegate_products_list, parent, false);

        Context context = itemView.getContext();

        RelativeLayout productBackground = itemView.findViewById(R.id.productRL);

        return new SelfDelegateProductsAdapter.MyViewHolder(itemView, productBackground);
    }

    @Override
    public void onBindViewHolder(SelfDelegateProductsAdapter.MyViewHolder holder, int position) {
        Product product = productList.get(position);

        holder.providerName.setText(product.getProviderName());
        holder.providerLocation.setText(product.getLocation());

        holder.selfDelegateButton.setOnClickListener(v -> {
            //Layouts to use in Self-Delegation Dialog + CountryCodePicker
            LinearLayout dialogLayoutMain = new LinearLayout(context);
            LinearLayout userNameLayout = new LinearLayout(context);
            LinearLayout userContactLayout = new LinearLayout(context);
            CountryCodePicker countryCodePicker = new CountryCodePicker(context);

            //Parameters to use for managing layout params
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            userNameLayout.setLayoutParams(params);

            //Styling layouts with padding of 10dp
            dialogLayoutMain.setOrientation(LinearLayout.VERTICAL);
            dialogLayoutMain.setPadding(10,10,10,10);
            userNameLayout.setOrientation(LinearLayout.HORIZONTAL);
            userNameLayout.setPadding(10,10,10,10);
            userContactLayout.setOrientation(LinearLayout.HORIZONTAL);
            userContactLayout.setPadding(10,10,10,10);
            //Configuring CountryCodePicker
            countryCodePicker.detectSIMCountry(true);
            countryCodePicker.detectNetworkCountry(true);
            countryCodePicker.setCcpDialogShowNameCode(true);
            countryCodePicker.setCcpDialogShowPhoneCode(false);
            countryCodePicker.getSelectedCountryName();
            //Init Dialog
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
//            AlertDialog.Builder builder1 = new AlertDialog.Builder(context);

            builder.setTitle("Enter User Details");

            final EditText userNameEditText = new EditText(context);
            final EditText userContactEditText = new EditText(context);
            final String[] userName = new String[1];
            final String[] userContact = new String[1];

            userNameEditText.setInputType(InputType.TYPE_CLASS_TEXT);
            userNameEditText.setHint("Name");
            userNameEditText.setLayoutParams(params);

            userContactEditText.setInputType(InputType.TYPE_CLASS_PHONE);
            userContactEditText.setHint("Contact");
            userContactEditText.setLayoutParams(params);

            //Inserting views into layouts
            userNameLayout.addView(userNameEditText);
            userContactLayout.addView(countryCodePicker);
            userContactLayout.addView(userContactEditText);
            dialogLayoutMain.addView(userNameLayout);
            dialogLayoutMain.addView(userContactLayout);

            builder.setView(dialogLayoutMain);
//            builder.setView(userContactEditText);

            builder.setPositiveButton("Send", (dialog, which) -> {
                userName[0] = userNameEditText.getText().toString();
                userContact[0] = userContactEditText.getText().toString();

                countryCodePicker.registerCarrierNumberEditText(userContactEditText);
                Log.e(TAG, "onClick: USER-NAME->"+userName[0]);
                Log.e(TAG, "onClick: USER-CONTACT->"+countryCodePicker.getFullNumberWithPlus());

                String agent_id = getUserId();
                ArrayList<String> progress = new ArrayList<>();
                procurementDocs = productList.get(position).getDocs();

                serviceProcesses = productList.get(position).getSteps();

                if (countryCodePicker.isValidFullNumber()) {

                    ProgressDialog progressDialog = new ProgressDialog(context);
                    progressDialog.setTitle("Sending to User");
                    progressDialog.setMessage("Please wait...");
                    progressDialog.show();

                    new SelfDelegate(countryCodePicker.getFullNumberWithPlus(), userName[0], productList.get(position).getId(), agent_id) {
                        @Override
                        public void done(@NotNull byte[] data, @NotNull Number code) {
                            Log.e(TAG, "done: SelfDelegating" + new String(data));
                            progressDialog.dismiss();

                            try {
                                JSONObject j = new JSONObject(new String(data));

                                //Clearing any previously stored 'Service-ID' before caching a new one
                                PreferenceManager.getDefaultSharedPreferences(context).edit().remove("SERVICE_ID").apply();

                                PreferenceManager.getDefaultSharedPreferences(context).edit().putString("SERVICE_ID", j.getString("_id")).apply();

                                ArrayList<String> docsList = new ArrayList<>();
                                if (procurementDocs != null) {
                                    for (int i = 0; i < procurementDocs.length(); i++) {
                                        docsList.add(procurementDocs.getString(i));
                                        delegatedServiceEntity.setDelegatedServiceDocs(docsList);
                                        Log.e(TAG, "procurementDocs->" + docsList);
                                    }
                                }
                                ArrayList<String> stepsList = new ArrayList<>();
                                if (serviceProcesses != null) {
                                    for (int k = 0; k < serviceProcesses.length(); k++) {
                                        stepsList.add(serviceProcesses.getString(k));
                                        delegatedServiceEntity.setDelegatedServiceSteps(stepsList);
                                        Log.e(TAG, "procurementSteps->" + stepsList);
                                    }
                                }

                                delegatedServiceViewModel = ViewModelProviders.of((FragmentActivity) context).get(DelegatedServiceViewModel.class);
                                delegatedServiceEntity.setDelegatedServiceId(j.getString("_id"));
                                delegatedServiceEntity.setDelegatedUserName(userName[0]);
                                delegatedServiceEntity.setDelegatedServiceName(productList.get(position).getProviderName());
                                delegatedServiceEntity.setDelegatedServiceDescription(productList.get(position).getDescription());
                                delegatedServiceEntity.setDelegatedServiceCost(productList.get(position).getCost());
                                delegatedServiceEntity.setDelegatedServiceDuration(productList.get(position).getDuration());
                                delegatedServiceEntity.setDelegatedServiceDocs(docsList);
                                delegatedServiceEntity.setDelegatedServiceSteps(stepsList);
                                delegatedServiceEntity.setDelegatedServiceProgress(progress);
                                delegatedServiceViewModel.insertDelegatedService(delegatedServiceEntity);

                                context.startActivity(new Intent(context, MainActivity.class)
                                        .putExtra("USER", userName[0])
                                        .putExtra("CONTACT", userContact[0]).putExtra("SERVICE_ID", j.getString("_id")));

                                Log.e(TAG, "done: " + delegatedServiceEntity.getDelegatedServiceId());
                                Log.e(TAG, "done: USER-NAME->" + Arrays.toString(userName));
                                Log.e(TAG, "done: DOCS->" + docsList);

                            } catch (JSONException e) {
                                Log.e("JSON-E", e.toString());
                            }
                            //Dismissing BottomSheet
                            //goToDelegatedService();
                        }
                    };
                } else{
                    Log.e(TAG, "onBindViewHolder: WRONG-NUMBER");
                    userContactEditText.setError("Incorrect contact");
                }

            });

            builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

            builder.show();

        });
    }

    private String getUserId(){
        try{
            String jwt =  PreferenceManager
                    .getDefaultSharedPreferences(this.context)
                    .getString("jwt", "")
                    .split(Pattern.quote("."))[1];
            return new JSONObject(new String(Base64.decode(jwt,Base64.DEFAULT))).getString("_id");
        }catch (JSONException jse){
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }
}
