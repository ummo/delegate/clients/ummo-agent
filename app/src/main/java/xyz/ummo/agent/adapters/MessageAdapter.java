package xyz.ummo.agent.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import xyz.ummo.agent.ChatBubble;
import xyz.ummo.agent.R;

public class MessageAdapter extends ArrayAdapter<ChatBubble> {

    private Activity activity;
    private List<ChatBubble> messages;

    public MessageAdapter(Activity context, int resource, List<ChatBubble> objects) {
        super(context, resource, objects);
        this.activity = context;
        this.messages = objects;
    }

    @NotNull
    @Override
    public View getView(int position, View convertView, @NotNull ViewGroup parent) {
        ViewHolder holder;
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        int layoutResource = 0; // determined by view type
        ChatBubble chatBubble = getItem(position);
        int viewType = getItemViewType(position);

        if (chatBubble != null) {
            if (chatBubble.myMessage()) {
                layoutResource = R.layout.left_chat_bubble;
            } else {
                layoutResource = R.layout.right_chat_bubble;
            }
        }
        convertView = inflater.inflate(layoutResource, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);

        //set message content
        if (chatBubble != null) {
            holder.msg.setText(chatBubble.getContent());
            holder.timeStamp.setText(chatBubble.getTimeStamp());
        }

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        // return the total number of view types. this value should never change
        // at runtime. Value 2 is returned because of left and right views.
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        // return a value between 0 and (getViewTypeCount - 1)
        return position % 2;
    }

    private class ViewHolder {
        private TextView msg;
        private TextView timeStamp;

        public ViewHolder(View v) {
            msg = v.findViewById(R.id.txt_msg);
            timeStamp = v.findViewById(R.id.message_time_stamp);
        }
    }
}
