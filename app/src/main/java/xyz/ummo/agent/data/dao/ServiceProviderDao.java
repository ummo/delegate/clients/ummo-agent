package xyz.ummo.agent.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import xyz.ummo.agent.data.entity.ServiceProviderEntity;

@Dao
public interface ServiceProviderDao {
    @Insert
    void insertServiceProvider(ServiceProviderEntity serviceProviderEntity);

    @Query("SELECT * FROM service_provider")
    LiveData<ServiceProviderEntity> getServiceProviderLiveData();

    @Update
    void updateServiceProvider(ServiceProviderEntity serviceProviderEntity);

    @Query("DELETE FROM service_provider")
    void deleteServiceProvider();
}
