package xyz.ummo.agent.data.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Objects;

import xyz.ummo.agent.data.entity.AgentEntity;

public class DataGenerator {
    private static String dataGenPrefs = "UMMO_AGENT_PREFERENCES";
    private static final int mode = Activity.MODE_PRIVATE;
    private static final String TAG = "DataGen";
    private static Context context;

    public static AgentEntity generateAgentEntity(){
        AgentEntity agentEntity = new AgentEntity();

        SharedPreferences mainActPreferences =  context.getSharedPreferences(dataGenPrefs, mode);
        String agentName = Objects.requireNonNull(mainActPreferences.getString("AGENT_NAME", ""));
        String agentContact = Objects.requireNonNull(mainActPreferences.getString("AGENT_CONTACT", ""));
        String agentEmail = Objects.requireNonNull(mainActPreferences.getString("AGENT_EMAIL", ""));
        boolean signed_up = mainActPreferences.getBoolean("SIGNED_UP",true);
        Log.e(TAG+ " onCreate", "Preferences: agent_name->"+ agentName);
        Log.e(TAG+ " onCreate", "Preferences: agent_contact->"+ agentContact);

        agentEntity.setName(agentName);
        agentEntity.setContact(agentContact);
        agentEntity.setEmail(agentEmail);
        String agentPIN = "";
        agentEntity.setPin(agentPIN);
        String agentKIN = "";
        agentEntity.setKin(agentKIN);
        return agentEntity;
    }
}
