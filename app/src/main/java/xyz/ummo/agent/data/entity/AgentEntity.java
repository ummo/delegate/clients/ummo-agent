package xyz.ummo.agent.data.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.OnConflictStrategy;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import xyz.ummo.agent.data.model.Agent;

@Entity(tableName = "agent")
public class AgentEntity implements Agent {

    @PrimaryKey(autoGenerate = true)
    private int agentId;

    @NonNull
    @ColumnInfo(name = "contact")
    private String contact = "Default Contact";

    @NonNull
    @ColumnInfo(name = "name")
    private String name = "Default Name";

    @NonNull
    @ColumnInfo(name = "email")
    private String email = "Default Email";

    @NonNull
    @ColumnInfo(name = "pin")
    private String pin = "No PIN";

    @NonNull
    @ColumnInfo(name = "kin")
    private String kin = "No KIN";

    /*@NonNull
    @ColumnInfo(name = "pid")
    private String pid = "No PID";*/

/*    @NonNull
    @ColumnInfo(name = "location")
    private JSONObject location;*/

    //TODO: Ensure this constructor gets implemented
    public AgentEntity(@NonNull String _contact,
                       @NonNull String _name,
                       @NonNull String _email,
                       @NonNull String _pin,
                       @NonNull String _kin
                      /* @NonNull String _pid,
                      @NonNull JSONObject _location*/){
        this.contact = _contact;
        this.name = _name;
        this.email = _email;
        this.pin = _pin;
        this.kin = _kin;
        /*this.pid = _pid*/
        /*this.location = _location*/
    }

    public AgentEntity(){}

    public int getAgentId() {
        return agentId;
    }

    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }

    @NotNull
    @Override
    public String getContact(){
        return this.contact;
    }

    public void setContact(@NotNull String _contact){this.contact = _contact;}

    @NotNull
    @Override
    public String getName(){
        return this.name;
    }

    public void setName(@NotNull String _name){this.name = _name;}

    @NotNull
    @Override
    public String getEmail(){
        return this.email;
    }

    public void setEmail(@NonNull String _email){this.email = _email;}

    @NotNull
    @Override
    public String getKin(){
        return this.kin;
    }

    public void setKin(@NonNull String _kin){this.kin = _kin;}

    @NotNull
    @Override
    public String getPin(){
        return this.pin;
    }

    public void setPin(@NonNull String _pin){this.pin = _pin;}

    /*@NonNull
    public String getPid() {
        return pid;
    }

    public void setPid(@NonNull String pid) {
        this.pid = pid;
    }*/

    /*@NotNull
    public JSONObject getLocation(){
        return this.location;
        //TODO: Confirm getter
    }*/
}
