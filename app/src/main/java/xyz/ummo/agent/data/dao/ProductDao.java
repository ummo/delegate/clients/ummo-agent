package xyz.ummo.agent.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import xyz.ummo.agent.data.entity.ProductEntity;

@Dao
public interface ProductDao {

    @Insert
    void insertProduct(ProductEntity productEntity);

    @Query("SELECT * FROM product")
    LiveData<ProductEntity> getProductLiveData();

    @Update
    void updateProduct(ProductEntity productEntity);

    @Query("DELETE FROM product")
    void deleteAllProducts();
}
