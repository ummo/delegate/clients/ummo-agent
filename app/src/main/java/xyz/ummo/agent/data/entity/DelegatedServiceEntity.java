package xyz.ummo.agent.data.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;

import xyz.ummo.agent.data.model.DelegatedService;

@Entity(tableName = "delegated_service")
public class DelegatedServiceEntity implements DelegatedService {

    @NonNull
    @PrimaryKey()
    @ColumnInfo(name = "delegated_service_id")
    private String delegatedServiceId;

    @NonNull
    @ColumnInfo(name = "delegated_user_id")
    private String delegatedUserName;

    @NonNull
    @ColumnInfo(name = "delegated_service_name")
    private String delegatedServiceName;

    @ColumnInfo(name = "delegated_service_description")
    private String delegatedServiceDescription;

    @ColumnInfo(name = "delegated_service_docs")
    private ArrayList<String> delegatedServiceDocs;

    @ColumnInfo(name = "delegated_service_cost")
    private String delegatedServiceCost;

    @ColumnInfo(name = "delegated_service_duration")
    private String delegatedServiceDuration;

    @ColumnInfo(name = "delegated_service_steps")
    private ArrayList<String> delegatedServiceSteps;

    public ArrayList<String> getDelegatedServiceProgress() {
        return delegatedServiceProgress;
    }

    public void setDelegatedServiceProgress(ArrayList<String> delegatedServiceProgress) {
        this.delegatedServiceProgress = delegatedServiceProgress;
    }

    @ColumnInfo(name = "delegated_service_progress")
    private ArrayList<String> delegatedServiceProgress;

    public DelegatedServiceEntity(@NonNull String _delegatedServiceId,
                                  @NonNull String _delegatedUserName,
                                  @NonNull String _delegatedServiceName,
                                  String _delegatedServiceDescription,
                                  @NonNull ArrayList<String> _delegatedServiceDocs,
                                  @NonNull String _delegatedServiceCost,
                                  @NonNull String _delegatedServiceDuration,
                                  @NonNull ArrayList<String> _delegatedServiceSteps,
                                  @NonNull ArrayList<String> _delegeatedServiceProgress){
        this.delegatedServiceId = _delegatedServiceId;
        this.delegatedUserName = _delegatedUserName;
        this.delegatedServiceName = _delegatedServiceName;
        this.delegatedServiceDescription = _delegatedServiceDescription;
        this.delegatedServiceDocs = _delegatedServiceDocs;
        this.delegatedServiceCost = _delegatedServiceCost;
        this.delegatedServiceDuration = _delegatedServiceDuration;
        this.delegatedServiceSteps = _delegatedServiceSteps;
        this.delegatedServiceProgress = _delegeatedServiceProgress;
    }

    public DelegatedServiceEntity(){}

    public String getDelegatedServiceId() {
        return delegatedServiceId;
    }

    public void setDelegatedServiceId(String delegatedServiceId) {
        this.delegatedServiceId = delegatedServiceId;
    }

    /*@NonNull
    @Override
    public String getDelegatedServiceId(){
        return this.delegatedServiceId;
    }

    public void setDelegatedServiceId(@NonNull String _delegatedServiceId){
        this.delegatedServiceId = _delegatedServiceId;
    }*/

    @NonNull
    @Override
    public String getDelegatedUserName() {
        return this.delegatedUserName;
    }

    public void setDelegatedUserName(@NonNull String _delegatedUserName){
        this.delegatedUserName = _delegatedUserName;
    }

    @NonNull
    @Override
    public String getDelegatedServiceName() {
        return this.delegatedServiceName;
    }

    public void setDelegatedServiceName(@NonNull String _delegatedServiceName){
        this.delegatedServiceName = _delegatedServiceName;
    }

    @NonNull
    @Override
    public String getDelegatedServiceDescription() {
        return this.delegatedServiceDescription;
    }

    public void setDelegatedServiceDescription(@NonNull String _delegatedServiceDescription){
        this.delegatedServiceDescription = _delegatedServiceDescription;
    }


    @NonNull
    @Override
    public String getDelegatedServiceCost() {
        return this.delegatedServiceCost;
    }

    public void setDelegatedServiceCost(@NonNull String _delegatedServiceCost){
        this.delegatedServiceCost = _delegatedServiceCost;
    }

    @NonNull
    @Override
    public String getDelegatedServiceDuration() {
        return this.delegatedServiceDuration;
    }

    public void setDelegatedServiceDuration(@NonNull String _delegatedServiceDuration){
        this.delegatedServiceDuration = _delegatedServiceDuration;
    }

    @Override
    public ArrayList<String> getDelegatedServiceDocs() {
        return delegatedServiceDocs;
    }

    public void setDelegatedServiceDocs(ArrayList<String> delegatedServiceDocs) {
        this.delegatedServiceDocs = delegatedServiceDocs;
    }

    @Override
    public ArrayList<String> getDelegatedServiceSteps() {
        return delegatedServiceSteps;
    }

    public void setDelegatedServiceSteps(ArrayList<String> delegatedServiceSteps) {
        this.delegatedServiceSteps = delegatedServiceSteps;
    }
}
