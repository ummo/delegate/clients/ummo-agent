package xyz.ummo.agent.data.repo;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

import xyz.ummo.agent.data.dao.AgentDao;
import xyz.ummo.agent.data.dao.DelegatedServiceDao;
import xyz.ummo.agent.data.dao.ProductDao;
import xyz.ummo.agent.data.dao.ServiceProviderDao;
import xyz.ummo.agent.data.db.AgentRoomDatabase;
import xyz.ummo.agent.data.entity.AgentEntity;
import xyz.ummo.agent.data.entity.DelegatedServiceEntity;
import xyz.ummo.agent.data.entity.ProductEntity;
import xyz.ummo.agent.data.entity.ServiceProviderEntity;
import xyz.ummo.agent.data.model.ServiceProvider;
import xyz.ummo.agent.delegate.CreateProduct;
import xyz.ummo.agent.delegate.GetProducts;
import xyz.ummo.agent.delegate.UpdateProgress;

public class AppRepository {

    private AgentDao agentDao;
    private DelegatedServiceDao delegatedServiceDao;
    private ProductDao productDao;
    private ServiceProviderDao serviceProviderDao;

    private LiveData<DelegatedServiceEntity> delegatedServiceEntityLiveData;
    private LiveData<AgentEntity> agentEntityLiveData;
    private LiveData<ProductEntity> productEntityLiveData;
    private LiveData<ServiceProviderEntity> serviceProviderEntityLiveData;

    private static final String TAG = "AppRepo";

    public AppRepository(Application application){
        AgentRoomDatabase agentRoomDatabase = AgentRoomDatabase.getAgentDatabase(application);

        agentDao = agentRoomDatabase.agentDao();
        agentEntityLiveData = agentDao.getAgentLiveData();
        Log.e("AppRepo", "Agent->"+ agentEntityLiveData);

        delegatedServiceDao = agentRoomDatabase.delegatedServiceDao();
        delegatedServiceEntityLiveData = delegatedServiceDao.getDelegatedService();
        Log.e("AppRepo", "DelegatedService->"+ agentEntityLiveData);

        productDao = agentRoomDatabase.productDao();
        productEntityLiveData = productDao.getProductLiveData();
        Log.e("AppRepo", "Product->"+productEntityLiveData);

        serviceProviderDao = agentRoomDatabase.serviceProviderDao();
        serviceProviderEntityLiveData = serviceProviderDao.getServiceProviderLiveData();
        Log.e("AppRepo", "serviceProvider->"+serviceProviderEntityLiveData);


    }

    public LiveData<DelegatedServiceEntity> getDelegatedServiceById(String delegatedServiceId){
        delegatedServiceEntityLiveData = delegatedServiceDao.getDelegatedServiceById(delegatedServiceId);
        return delegatedServiceEntityLiveData;
    }

    public LiveData<DelegatedServiceEntity> getAllDelegatedServices(){
        delegatedServiceEntityLiveData = delegatedServiceDao.getAllDelegatedServices();
        return delegatedServiceEntityLiveData;
    }

    /*private AppRepository(final AgentRoomDatabase agentRoomDatabase){
        database = agentRoomDatabase;

    }*/

    /**
     * Repository API call for CRUDING Agent. We use the DAO to abstract the connection to the
     * AgentEntity. DAO calls implemented are C-InsertAgent; R-LiveData (exempt from AsyncOps); U-UpdateAgent && D-DeleteAgent.
     * Each are done asynchronously because RoomDB does not run on the main thread
     * */

    public void insertAgent(AgentEntity agentEntity){
        new insertAgentAsyncTask(agentDao).execute(agentEntity);
    }

    public void deleteAgent(){
        new deleteAgentAsyncTask(agentDao).execute();
    }

    public LiveData<AgentEntity> getAgentEntityLiveData(){
        Log.e("AppRepo", "Agent LiveData->"+ agentEntityLiveData);
        return agentEntityLiveData;
    }

    public void updateAgent(AgentEntity agentEntity){
        new updateAgentAsyncTask(agentDao).execute(agentEntity);
    }

    private static class insertAgentAsyncTask extends AsyncTask<AgentEntity, Void, Void>{
        private AgentDao mAgentAsyncTaskDao;

        private insertAgentAsyncTask(AgentDao agentDao){
            this.mAgentAsyncTaskDao = agentDao;
        }

        @Override
        protected Void doInBackground(final AgentEntity... agentEntities) {
            mAgentAsyncTaskDao.insertAgent(agentEntities[0]);
            Log.e("AppRepo", "Inserting Agent->"+ Arrays.toString(agentEntities));
            return null;
        }
    }

    private static class deleteAgentAsyncTask extends AsyncTask<Void, Void, Void>{
        private AgentDao mAgentAsyncTaskDao;

        deleteAgentAsyncTask(AgentDao agentDao){
            this.mAgentAsyncTaskDao = agentDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAgentAsyncTaskDao.deleteAgent();
            Log.e("AppRepo", "Deleting Agent!");
            return null;
        }
    }

    private static class updateAgentAsyncTask extends AsyncTask<AgentEntity, Void, Void>{
        private AgentDao mAgentAsyncTaskDao;

        updateAgentAsyncTask(AgentDao agentDao){
            mAgentAsyncTaskDao = agentDao;
        }

        @Override
        protected Void doInBackground(final AgentEntity... agentEntities) {
            mAgentAsyncTaskDao.updateAgent(agentEntities[0]);
            Log.e("AppRepo", "Updating Agent->"+ Arrays.toString(agentEntities));
            return null;
        }
    }

    /**
     * Repository API call for CRUDING DelegatedService. We use the DAO to abstract the connection to the
     * DelegatedEntity. DAO calls implemented are C-InsertDelegatedService; R-LiveData (exempt from AsyncOps); U-UpdateDelegatedService && D-DeleteDelegatedService.
     * Each are done asynchronously because RoomDB does not run on the main thread
     **/

    public void insertDelegatedService(DelegatedServiceEntity delegatedServiceEntity){
        new insertDelegatedServiceAsyncTask(delegatedServiceDao).execute(delegatedServiceEntity);
    }

    public LiveData<DelegatedServiceEntity> getDelegatedServiceEntityLiveData(){
        Log.e("AppRepo", "Agent LiveData->"+ agentEntityLiveData);
        return delegatedServiceEntityLiveData;
    }

    public void deleteAllDelegatedServices(){
        new deleteAllDelegatedServicesAsyncTask(delegatedServiceDao).execute();
    }

    public void updateDelegatedService(DelegatedServiceEntity delegatedServiceEntity){
        new UpdateProgress(delegatedServiceEntity.getDelegatedServiceId(),delegatedServiceEntity.getDelegatedServiceProgress()){
            @Override
            public void done(@NotNull byte[] data, @NotNull Number code) {
                new updateDelegatedServiceAsyncTask(delegatedServiceDao).execute(delegatedServiceEntity);
            }
        };
    }

    private static class insertDelegatedServiceAsyncTask extends AsyncTask<DelegatedServiceEntity, Void, Void>{
        private DelegatedServiceDao mDelegatedServiceDao;

        private insertDelegatedServiceAsyncTask(DelegatedServiceDao delegatedServiceDao){
            this.mDelegatedServiceDao = delegatedServiceDao;
        }

        @Override
        protected Void doInBackground(final DelegatedServiceEntity... delegatedServiceEntities) {
            mDelegatedServiceDao.insertDelegatedService(delegatedServiceEntities[0]);
            Log.e("AppRepo", "Inserting Delegated Service->"+ Arrays.toString(delegatedServiceEntities));
            return null;
        }
    }

    private static class deleteAllDelegatedServicesAsyncTask extends AsyncTask<Void, Void, Void>{
        private DelegatedServiceDao mDelegatedServiceAsyncTaskDao;

        deleteAllDelegatedServicesAsyncTask(DelegatedServiceDao delegatedServiceDao){
            this.mDelegatedServiceAsyncTaskDao = delegatedServiceDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mDelegatedServiceAsyncTaskDao.deleteAllDelegatedServices();
            Log.e("AppRepo", "Deleting Delegated Service!");
            return null;
        }
    }

    private static class updateDelegatedServiceAsyncTask extends AsyncTask<DelegatedServiceEntity, Void, Void>{
        private DelegatedServiceDao mDelegatedServiceAsyncTaskDao;

        updateDelegatedServiceAsyncTask(DelegatedServiceDao delegatedServiceDao){
            mDelegatedServiceAsyncTaskDao = delegatedServiceDao;
        }

        @Override
        protected Void doInBackground(final DelegatedServiceEntity... delegatedServiceEntities) {
            mDelegatedServiceAsyncTaskDao.updateDelegatedService(delegatedServiceEntities[0]);
            Log.e("AppRepo", "Updating Delegated Service->"+ Arrays.toString(delegatedServiceEntities));
            return null;
        }
    }

    /**
     * Repository API call for CRUDING Service Provider. We use the DAO to abstract the connection to the
     * ServiceProviderEntity. DAO calls implemented are C-InsertServiceProvider; R-LiveData (exempt from AsyncOps); U-UpdateServiceProvider && D-DeleteServiceProvider.
     * Each are done asynchronously because RoomDB does not run on the main thread
     **/

    public void insertServiceProvider(ServiceProviderEntity serviceProviderEntity){
        new insertServiceProviderAsyncTask(serviceProviderDao).execute(serviceProviderEntity);
    }

    public LiveData<ServiceProviderEntity> getServiceProviderEntityLiveData(){
        Log.e("AppRepo", "Service Provider LiveData->"+ serviceProviderEntityLiveData);
        return serviceProviderEntityLiveData;
    }

    public void deleteServiceProvider(){
        new deleteServiceProviderAsyncTask(serviceProviderDao).execute();
    }

    public void updateServiceProvider(ServiceProviderEntity serviceProviderEntity){
        new updateServiceProviderAsyncTask(serviceProviderDao).execute(serviceProviderEntity);
    }

    private static class insertServiceProviderAsyncTask extends AsyncTask<ServiceProviderEntity, Void, Void>{
        private ServiceProviderDao mServiceProviderDao;

        private insertServiceProviderAsyncTask(ServiceProviderDao serviceProviderDao){
            this.mServiceProviderDao = serviceProviderDao;
        }

        @Override
        protected Void doInBackground(final ServiceProviderEntity... serviceProviderEntities) {
            mServiceProviderDao.insertServiceProvider(serviceProviderEntities[0]);
            Log.e("AppRepo", "Inserting Service Provider->"+ Arrays.toString(serviceProviderEntities));
            return null;
        }
    }

    private static class deleteServiceProviderAsyncTask extends AsyncTask<Void, Void, Void>{
        private ServiceProviderDao mServiceProviderAsyncTaskDao;

        deleteServiceProviderAsyncTask(ServiceProviderDao serviceProviderDao){
            this.mServiceProviderAsyncTaskDao = serviceProviderDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mServiceProviderAsyncTaskDao.deleteServiceProvider();
            Log.e("AppRepo", "Deleting Service Provider!");
            return null;
        }
    }

    private static class updateServiceProviderAsyncTask extends AsyncTask<ServiceProviderEntity, Void, Void>{
        private ServiceProviderDao mServiceProviderAsyncTaskDao;

        updateServiceProviderAsyncTask(ServiceProviderDao serviceProviderDao){
            mServiceProviderAsyncTaskDao = serviceProviderDao;
        }

        @Override
        protected Void doInBackground(final ServiceProviderEntity... serviceProviderEntities) {
            mServiceProviderAsyncTaskDao.updateServiceProvider(serviceProviderEntities[0]);
            Log.e("AppRepo", "Updating Service Provider->"+ Arrays.toString(serviceProviderEntities));
            return null;
        }
    }
    /**
     * Repository API call for CRUDING Product. We use the DAO to abstract the connection to the
     * ProductEntity. DAO calls implemented are C-InsertProduct; R-LiveData (exempt from AsyncOps); U-UpdateProduct && D-DeleteProduct.
     * Each are done asynchronously because RoomDB does not run on the main thread
     **/

    public void insertProduct(ProductEntity productEntity){
        new insertProductAsyncTask(productDao).execute(productEntity);
    }

    public LiveData<ProductEntity> getProductEntityLiveData(){
        Log.e("AppRepo", "Product LiveData->"+productEntityLiveData);
        return productEntityLiveData;
    }

    public void deleteAllProducts(){
        new deleteAllProductsAsyncTask(productDao).execute();
    }

    public void updateProduct(ProductEntity productEntity){
        new updateProductAsyncTask(productDao).execute(productEntity);
    }

    private static class insertProductAsyncTask extends AsyncTask<ProductEntity, Void, Void>{
        private ProductDao mProductDao;

        private insertProductAsyncTask(ProductDao productDao){
            this.mProductDao = productDao;
        }

        @Override
        protected Void doInBackground(final ProductEntity... productEntities) {
            mProductDao.insertProduct(productEntities[0]);
            Log.e(TAG, "Inserting Product->"+ productEntities[0].getProductName());

            new CreateProduct(
                productEntities[0].getProductName()
                    ,productEntities[0].getProductDescription(),
                    productEntities[0].getProductSteps(),
                    false,
                    productEntities[0].getProductCost(),
                    productEntities[0].getProductDocuments(),
                    productEntities[0].getProductProvider(),
                    productEntities[0].getProductDuration()){

            @Override
            public void done(@NotNull byte[] data, @NotNull Number code) {
                Log.e(TAG, "Inserting Product data->"+ Arrays.toString(data));
                Log.e(TAG, "Inserting Product code->"+ code);
            }
        };
            return null;
        }
    }

    private static class deleteAllProductsAsyncTask extends AsyncTask<Void, Void, Void>{
        private ProductDao mProductAsyncTaskDao;

        deleteAllProductsAsyncTask(ProductDao productDao){
            this.mProductAsyncTaskDao = productDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mProductAsyncTaskDao.deleteAllProducts();
            Log.e("AppRepo", "Deleting Product!");
            return null;
        }
    }

    private static class updateProductAsyncTask extends AsyncTask<ProductEntity, Void, Void>{
        private ProductDao mProductAsyncTaskDao;

        updateProductAsyncTask(ProductDao productDao){
            mProductAsyncTaskDao = productDao;
        }

        @Override
        protected Void doInBackground(final ProductEntity... productEntities) {
            mProductAsyncTaskDao.updateProduct(productEntities[0]);
            Log.e("AppRepo", "Updating Product->"+ Arrays.toString(productEntities));
            return null;
        }
    }

   /* GetProducts getProducts = new GetProducts() {
        @Override
        public void done(@NotNull byte[] data, @NotNull Number code) {
            if (code.equals(200)){
                Log.e(TAG+" GetProducts", "Logging Products->"+new String(data));
            } else {
                Log.e(TAG+" GetProducts", "WTF happened->"+code);
                logWithStaticAPI();
            }
        }
    };*/

    private void unsafeMethod(){
        throw new UnsupportedOperationException("This needs attention!");
    }
}
