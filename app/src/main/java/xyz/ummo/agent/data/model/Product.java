package xyz.ummo.agent.data.model;


import java.util.ArrayList;

public interface Product {
    String getProductName();
    String getProductDescription();
    String getProductProvider();
    ArrayList<String> getProductDocuments();
    String getProductCost();
    String getProductDuration();
    ArrayList<String> getProductSteps();
}
