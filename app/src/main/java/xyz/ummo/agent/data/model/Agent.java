package xyz.ummo.agent.data.model;

public interface Agent {
    String getName();
    String getContact();
    String getEmail();
    String getPin();
    String getKin();
//    String getPid();
}
