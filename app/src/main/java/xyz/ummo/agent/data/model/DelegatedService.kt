package xyz.ummo.agent.data.model

import java.util.ArrayList

interface DelegatedService {
    //    String getDelegatedServiceId();
    val delegatedUserName: String?
    val delegatedServiceName: String?
    val delegatedServiceDescription: String?
    val delegatedServiceDocs: ArrayList<String?>?
    val delegatedServiceCost: String?
    val delegatedServiceDuration: String?
    val delegatedServiceSteps: ArrayList<String?>?
    val delegatedServiceProgress: ArrayList<String?>?
}