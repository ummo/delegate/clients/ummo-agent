package xyz.ummo.agent.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import xyz.ummo.agent.data.entity.AgentEntity;

@Dao
public interface AgentDao {

    @Insert
    void insertAgent(AgentEntity agentEntity);

    @Query("SELECT * from agent")
    LiveData<AgentEntity> getAgentLiveData();

    @Update
    void updateAgent(AgentEntity agentEntity);

    @Query("DELETE FROM agent")
    void deleteAgent();
}
