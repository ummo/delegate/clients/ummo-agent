package xyz.ummo.agent.data.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import xyz.ummo.agent.data.model.ServiceProvider;

@Entity(tableName = "service_provider")
public class ServiceProviderEntity implements ServiceProvider {
    @PrimaryKey(autoGenerate = true)
    private int serviceProviderId;

    @NonNull
    @ColumnInfo(name = "service_provider_name")
    private String serviceProviderName = "Default Name";

    @NonNull
    @ColumnInfo(name = "service_provider_province")
    private String serviceProviderProvince = "Default Province";

    @NonNull
    @ColumnInfo(name = "service_provider_municipality")
    private String serviceProviderMunicipality = "Default Municipality";

    @NonNull
    @ColumnInfo(name = "service_provider_town")
    private String serviceProviderTown = "Default Town";

    public ServiceProviderEntity(@NonNull String _serviceProviderName,
                                 @NonNull String _serviceProviderProvince,
                                 @NonNull String _serviceProviderMunicipality,
                                 @NonNull String _serviceProviderTown){
        this.serviceProviderName = _serviceProviderName;
        this.serviceProviderProvince = _serviceProviderProvince;
        this.serviceProviderMunicipality = _serviceProviderMunicipality;
        this.serviceProviderTown = _serviceProviderTown;
    }

    public ServiceProviderEntity(){}

    public int getServiceProviderId(){return serviceProviderId;}

    public  void  setServiceProviderId(int serviceProviderId){
        this.serviceProviderId = serviceProviderId;
    }

    @Override
    @NonNull
    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(@NonNull String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    @Override
    @NonNull
    public String getServiceProviderProvince() {
        return serviceProviderProvince;
    }

    public void setServiceProviderProvince(@NonNull String serviceProviderProvince) {
        this.serviceProviderProvince = serviceProviderProvince;
    }

    @Override
    @NonNull
    public String getServiceProviderMunicipality() {
        return serviceProviderMunicipality;
    }

    public void setServiceProviderMunicipality(@NonNull String serviceProviderMunicipality) {
        this.serviceProviderMunicipality = serviceProviderMunicipality;
    }

    @Override
    @NonNull
    public String getServiceProviderTown() {
        return serviceProviderTown;
    }

    public void setServiceProviderTown(@NonNull String serviceProviderTown) {
        this.serviceProviderTown = serviceProviderTown;
    }
}
