package xyz.ummo.agent.data.entity;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.ArrayList;
import java.util.List;

import xyz.ummo.agent.data.model.Product;

@Entity(tableName = "product")
public class ProductEntity implements Product {
    @PrimaryKey(autoGenerate = true)
    private int productId;

    @NonNull
    @ColumnInfo(name = "product_name")
    private String productName = "Default Name";

    @NonNull
    @ColumnInfo(name = "product_description")
    private String productDescription = "Default Description";

    @NonNull
    @ColumnInfo(name = "product_provider")
    private String productProvider = "Default Provider";

    @NonNull
    @ColumnInfo(name = "product_documents")
    private ArrayList<String> productDocuments = new ArrayList<>();

    @NonNull
    @ColumnInfo(name = "product_cost")
    private String productCost = "Default Cost";

    @NonNull
    @ColumnInfo(name = "product_duration")
    private String productDuration = "Default Duration";

    @NonNull
    @ColumnInfo(name = "Default Steps")
    private ArrayList<String> productSteps = new ArrayList<>();

    public ProductEntity(@NonNull String _productName,
                         @NonNull String _productDescription,
                         @NonNull String _productProvider,
                         @NonNull ArrayList<String> _productDocuments,
                         @NonNull String _productCost,
                         @NonNull String _productDuration,
                         @NonNull ArrayList<String> _productSteps){
        this.productName = _productName;
        this.productDescription = _productDescription;
        this.productProvider = _productProvider;
        this.productDocuments = _productDocuments;
        this.productCost = _productCost;
        this.productDuration = _productDuration;
        this.productSteps = _productSteps;
    }

    public ProductEntity(){}

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    @Override
    @NonNull
    public String getProductName() {
        return productName;
    }

    public void setProductName(@NonNull String productName) {
        this.productName = productName;
    }

    @Override
    @NonNull
    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(@NonNull String productDescription) {
        this.productDescription = productDescription;
    }

    @Override
    @NonNull
    public String getProductProvider() {
        return productProvider;
    }

    public void setProductProvider(@NonNull String productProvider) {
        this.productProvider = productProvider;
    }

    @Override
    @NonNull
    public ArrayList<String> getProductDocuments() {
        return productDocuments;
    }

    public void setProductDocuments(@NonNull ArrayList<String> productDocuments) {
        this.productDocuments = productDocuments;
    }

    public void setProductSteps(@NonNull ArrayList<String> productSteps) {
        this.productSteps = productSteps;
    }

    @Override
    @NonNull
    public ArrayList<String> getProductSteps() {
        return productSteps;
    }

    @Override
    @NonNull
    public String getProductCost() {
        return productCost;
    }

    public void setProductCost(@NonNull String productCost) {
        this.productCost = productCost;
    }

    @Override
    @NonNull
    public String getProductDuration() {
        return productDuration;
    }

    public void setProductDuration(@NonNull String productDuration) {
        this.productDuration = productDuration;
    }
}
