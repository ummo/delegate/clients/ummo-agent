package xyz.ummo.agent.data.db;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import xyz.ummo.agent.data.utils.Converters;
import xyz.ummo.agent.data.dao.AgentDao;
import xyz.ummo.agent.data.dao.DelegatedServiceDao;
import xyz.ummo.agent.data.dao.ProductDao;
import xyz.ummo.agent.data.dao.ServiceProviderDao;
import xyz.ummo.agent.data.entity.AgentEntity;
import xyz.ummo.agent.data.entity.DelegatedServiceEntity;
import xyz.ummo.agent.data.entity.ProductEntity;
import xyz.ummo.agent.data.entity.ServiceProviderEntity;

import static androidx.room.Room.databaseBuilder;

@Database(entities = {AgentEntity.class,
                      DelegatedServiceEntity.class,
                      ProductEntity.class,
                      ServiceProviderEntity.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AgentRoomDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "UMMO-AGENT-DB";
    private final MutableLiveData<Boolean> mIsDatabaseCreated = new MutableLiveData<>();

    public abstract AgentDao agentDao();
    public abstract DelegatedServiceDao delegatedServiceDao();
    public abstract ProductDao productDao();
    public abstract ServiceProviderDao serviceProviderDao();
    private static volatile AgentRoomDatabase INSTANCE;

/*    public static AgentRoomDatabase getInstance(final Context context,
                                                final AppExecutors appExecutors){
        if (INSTANCE == null){
            synchronized (AgentRoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = buildDatabase(context, appExecutors);
                    INSTANCE.updateDatabaseCreated(context.getApplicationContext());
                }
            }
        }
        return INSTANCE;
    }*/

    public static AgentRoomDatabase getAgentDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (AgentRoomDatabase.class){
                //Create database here
                INSTANCE = databaseBuilder(context.getApplicationContext(),
                        AgentRoomDatabase.class,
                        "agent_database")
                        .addCallback(roomDatabaseCallback)
                        .build();
            }
        }
        return INSTANCE;
    }

    /*private static AgentRoomDatabase buildDatabase(final Context context, final AppExecutors executors){
        return Room.databaseBuilder(context, AgentRoomDatabase.class, DATABASE_NAME)
                .addCallback(new Callback() {
                    @Override
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        super.onCreate(db);
                        executors.diskIO().execute(() -> {
                            // Add a delay to simulate a long-running operation
                            // Generate the data for pre-population
                            AgentRoomDatabase database = AgentRoomDatabase.getInstance();
                            AgentEntity agentEntity = DataGenerator.generateAgentEntity();
                            *//*List<ProductEntity> products = DataGenerator.generateProducts();
                            List<CommentEntity> comments =
                                    DataGenerator.generateCommentsForProducts(products);*//*

                            insertAgentData(database, agentEntity);
                            // notify that the database was created and it's ready to be used
                            database.setDatabaseCreated();
                        });
                    }
                })
                .build();
    }*/

    private void setDatabaseCreated(){mIsDatabaseCreated.postValue(true);}

    /**
     * Check whether the database already exists and expose it via {@link #getDatabaseCreated()}
     */
    private void updateDatabaseCreated(final Context context) {
        if (context.getDatabasePath(DATABASE_NAME).exists()) {
            setDatabaseCreated();
        }
    }

    public LiveData<Boolean> getDatabaseCreated() {
        return mIsDatabaseCreated;
    }

    private static void insertAgentData(final AgentRoomDatabase database, final AgentEntity agentEntity){
        database.runInTransaction(() -> {
            database.agentDao().insertAgent(agentEntity);
        });
    }

    private static RoomDatabase.Callback roomDatabaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new PopulateDbAsync(INSTANCE).execute();
        }
    };

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void>{
        private final AgentDao agentDao;
        private DelegatedServiceDao delegatedServiceDao;
        private ProductDao productDao;
        private ServiceProviderDao serviceProviderDao;

        private PopulateDbAsync(AgentRoomDatabase agentRoomDatabase){
            agentDao = agentRoomDatabase.agentDao();
            delegatedServiceDao = agentRoomDatabase.delegatedServiceDao();
            productDao = agentRoomDatabase.productDao();
            serviceProviderDao = agentRoomDatabase.serviceProviderDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
//            agentDao.deleteAgent();
            /*agentDao.insertAgent(new AgentEntity("76157688","Jose","rego@ummo.xyz",
                    "920514","Carlos"));*/

/*            delegatedServiceDao.insertDelegatedService(new DelegatedServiceEntity("12345",
                    "4321",
                    "Licence Renewal", "This is it...",
                    "Doc 1 & Form 1", "R150",
                    "4 hours","Step 1; Step 2; Step 3"));*/
            return null;
        }
    }
}
