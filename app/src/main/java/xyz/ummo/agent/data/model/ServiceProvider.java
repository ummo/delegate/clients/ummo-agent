package xyz.ummo.agent.data.model;

public interface ServiceProvider {
    String getServiceProviderName();
    String getServiceProviderProvince();
    String getServiceProviderMunicipality();
    String getServiceProviderTown();
    //TODO:String getServiceProviderLatLong()
}
