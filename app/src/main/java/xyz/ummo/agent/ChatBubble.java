package xyz.ummo.agent;

public class ChatBubble {

    private String content;
    private String timeStamp;
    private boolean myMessage;

    public ChatBubble(String content, String timeStamp, boolean myMessage) {
        this.content = content;
        this.timeStamp = timeStamp;
        this.myMessage = myMessage;
    }

    public String getContent() {
        return content;
    }

    public String getTimeStamp(){
        return timeStamp;
    }

    public boolean myMessage() {
        return myMessage;
    }
}
