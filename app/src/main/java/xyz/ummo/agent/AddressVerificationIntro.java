package xyz.ummo.agent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AddressVerificationIntro extends AppCompatActivity {

    Button proceed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_verification_intro);
        setTitle("Address Verification");

        proceed =  findViewById(R.id.proceed_btn);

        proceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                intentToAddressVerific();
            }
        });
    }


    private void intentToAddressVerific(){


        Intent i = new Intent(this, AddressVerification.class);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.push_left_in,R.anim.push_left_out);

    }
}
