package xyz.ummo.agent.delegate

import android.app.Activity
import android.content.Context
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import org.json.JSONArray
import org.json.JSONObject

abstract class UpdateProgress(serviceId:String, progress:List<String>) {
    init {
        val obj = JSONObject()
        obj.put("progress",toJSONArray(progress))
        obj.put("_id",serviceId)
        Fuel.put("/service/${serviceId}")
                .jsonBody(obj.toString())
                .response { request, response, result ->
                        done(response.data,response.statusCode)

                }
    }

    private fun toJSONArray(x: List<String>): JSONArray {
        val arr = JSONArray()
        x.forEach { arr.put(it) }
        return arr
    }
    abstract fun done(data:ByteArray,code:Number)
}