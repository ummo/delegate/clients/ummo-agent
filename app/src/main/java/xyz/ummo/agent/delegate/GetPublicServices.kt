package xyz.ummo.agent.delegate

import com.github.kittinunf.fuel.Fuel
import timber.log.Timber

abstract class GetPublicServices {
    init {
        Fuel.get("/public-service")
                .response { request, response, result ->
                    done(response.data, response.statusCode)
                    Timber.e("Public Services->${response.data} Status Code->${response.statusCode}")
                }
    }

    abstract fun done(data:ByteArray,code:Number)
}