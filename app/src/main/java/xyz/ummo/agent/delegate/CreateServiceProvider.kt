package xyz.ummo.agent.delegate

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import org.json.JSONObject
import timber.log.Timber

abstract class CreateServiceProvider(service_name: String, province: String, municipality: String, town: String) {
    init {
        val _body = JSONObject()
                .put("service_name", service_name)
                .put("location", JSONObject()
                        .put("province", province)
                        .put("municipality", municipality)
                        .put("town", town))

        Fuel.post("/public-service")
                .jsonBody(_body.toString())
                .response { request, response, result ->
                    println(response)
                    done(response.data, response.statusCode)
                }

    }

    abstract fun done(data: ByteArray, code: Number)
}