package xyz.ummo.agent.delegate

import android.content.Context
import android.content.Intent
import android.preference.PreferenceManager
import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.github.nkzawa.emitter.Emitter
import org.json.JSONObject
import timber.log.Timber
import xyz.ummo.agent.ui.DelegationChat
import xyz.ummo.agent.ui.MainActivity

abstract class Login(context: Context, name: String, email: String, password: String, mobile_contact: String, player_id: String) {
    init {
        val agent = JSONObject()
                .put("name", name)
                .put("email", email)
                .put("password", password)
                .put("contact", mobile_contact)
                .put("agent_pid", player_id)

        Fuel.post("/admin/login")
                .jsonBody(agent.toString())
                .response { request, response, result ->

                    if (response.statusCode==200){
//                        val jwt = response.headers.get("Jwt").get(0).toString()
                        val jwt = response.headers["Jwt"].elementAt(0).toString() //TODO: Expect an error from missing JWT!
                        FuelManager.instance.baseHeaders = mapOf("jwt" to jwt)

                        SocketIO.mSocket?.on("connect_error", Emitter.Listener {
                            Timber.e("SOCKET - CONNECTION_ERROR -> ${it[0].toString()+SocketIO.mSocket?.io()}")
                        })

                        /*if(SocketIO.mSocket?.connected()!!){
                            //Timber.e("SOCKET - CONNECTED -> ${SocketIO.mSocket}") TODO: investigate error
                        }else{
                            Timber.e("SOCKET - NOT CONNECTED -> ${SocketIO.mSocket}")
                        }*/

                        SocketIO.mSocket?.on("service-created", Emitter.Listener {
                            val intent = Intent(context, DelegationChat::class.java)
                            intent.putExtra("service-created",it[0].toString())
                            context.startActivity(intent)
                        })

                        SocketIO.mSocket?.on("dispatch", Emitter.Listener {
                            val intent = Intent(context, MainActivity::class.java)
                            Timber.e("SOCKET - DISPATCH_CALLED -> ${it[0]}")

                            intent.putExtra("dispatch",it[0].toString())
                            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                            context.startActivity(intent)
                        })

                        SocketIO.mSocket?.on("service-done", Emitter.Listener {
                            //TODO: Start activity to handle service done
                            Timber.e("SOCKET - SERVICE_DONE -> ${it[0]}")
                        })

                        PreferenceManager
                                .getDefaultSharedPreferences(context)
                                .edit()
                                .putString("jwt",jwt)
                                .apply()
                    }
                    println(response)
                    Timber.e("RESPONSE -> $response")
                    done(response.data, response.statusCode)
                }
    }

    abstract fun done(data:ByteArray,code:Number)
}