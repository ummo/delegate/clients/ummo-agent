package xyz.ummo.agent.delegate

import android.app.Activity
import com.github.kittinunf.fuel.Fuel

abstract class GetProducts(activity:Activity) {
    init {
        Fuel.get("/product")
                .response { request, response, result ->
                    activity.runOnUiThread {
                        done(response.data, response.statusCode)
                    }
                }
    }

    abstract fun done(data:ByteArray,code:Number)
}