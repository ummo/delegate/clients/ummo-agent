package xyz.ummo.agent.delegate

import android.app.Application
import android.content.Intent
import android.preference.PreferenceManager
import android.util.Base64
import android.util.Log
import com.github.kittinunf.fuel.core.FuelManager
import com.onesignal.OneSignal
import xyz.ummo.agent.R
import java.net.URISyntaxException
import com.github.nkzawa.emitter.Emitter
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.onesignal.OSNotificationAction
import com.onesignal.OSNotificationOpenResult
import org.json.JSONObject
import timber.log.Timber
import xyz.ummo.agent.BuildConfig
import xyz.ummo.agent.ui.DelegationChat
import xyz.ummo.agent.ui.MainActivity
import xyz.ummo.agent.utilities.CustomNotificationOpenHandler


class Agent : Application(){

    private val instance: Agent? = null

    init {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    override fun onCreate() {
        super.onCreate()
        AgentObject.agent = this
        FuelManager.instance.basePath = getString(R.string.serverUrl)

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationOpenedHandler(CustomNotificationOpenHandler())
                .init()

        val jwt:String = PreferenceManager.getDefaultSharedPreferences(this).getString("jwt","").toString()
        val service = PreferenceManager.getDefaultSharedPreferences(this).getString("SERVICE_ID","")

        if(jwt!=""){
            FuelManager.instance.baseHeaders = mapOf("jwt" to jwt)

            initializeSocket(getAgentId(jwt))

            Timber.e("AGENT-ID -> ${getAgentId(jwt)}")

            SocketIO.mSocket?.connect()

            Timber.e(SocketIO.anything)

            SocketIO.mSocket?.on("connect_error") {
                Timber.e("Socket Connect Error -? ${it[0]} + ${SocketIO.mSocket?.io()}")
            }

            if(SocketIO.mSocket?.connected()!!){
                Timber.e("Hurray")
            }else{
                Timber.e("Boohooo ${SocketIO.mSocket}")
            }

            SocketIO.mSocket?.on("service-created") {
                val intent = Intent(this, DelegationChat::class.java)
                intent.putExtra("service-created",it[0].toString())
                startActivity(intent)
            }

            SocketIO.mSocket?.on("dispatch") {
                val intent = Intent(this, MainActivity::class.java)
                Timber.e("Dispatch Called ${it[0]}...")

                intent.putExtra("dispatch",it[0].toString())
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }

            SocketIO.mSocket?.on("service-done") {
                //TODO: Start activity to handle service done
                Timber.e("Hooray service is done")
            }

        }else{
            Timber.e("No JWT")
        }

        Timber.e("Application created")
    }

    /*private val onNewDispatch = Emitter.Listener { args ->
        getActivity().runOnUiThread(Runnable {
            val data = args[0] as JSONObject
            val username: String
            val message: String
            try {
                username = data.getString("username")
                message = data.getString("message")
            } catch (e: JSONException) {
                return@Runnable
            }
        })
    }*/

    private fun initializeSocket(_id:String){
        try {
            Timber.e("Trying connection")
            SocketIO.mSocket = IO.socket("${getString(R.string.serverUrl)}/agent-$_id")
            SocketIO.mSocket?.connect()
            SocketIO.anything = "Hello World"

            if(SocketIO.mSocket==null){
                Timber.e("Probably not connected")
            }else{
                Timber.e("Probably connected")
            }
        } catch (e: URISyntaxException) {
            Timber.e(e.toString())
        }
    }

    companion object{

        fun getAgentId(_jwt: String): String { //Remember, it takes a jwt string
            return JSONObject(String(Base64.decode(_jwt.split(".")[1], Base64.DEFAULT))).getString("_id")
        }
    }

}

object AgentObject{
    var agent: Agent? = null
}

object SocketIO{
    var mSocket: Socket? = null
    var anything = ""

}
