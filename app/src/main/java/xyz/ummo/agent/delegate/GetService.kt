package xyz.ummo.agent.delegate

import android.util.Log
import com.github.kittinunf.fuel.Fuel
import timber.log.Timber

abstract class GetService(_id: String) {
    init {
        Timber.e("_ID -> $_id")

        Fuel.get("/service/$_id")
                .response { request, response, result ->
                    done(response.data, response.statusCode)
                    Timber.e("Services-> ${response.data} Status Code->${response.statusCode}")
                }
    }

    abstract fun done(data: ByteArray, code: Number)
}