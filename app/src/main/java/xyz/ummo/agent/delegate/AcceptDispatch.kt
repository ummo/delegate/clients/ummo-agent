package xyz.ummo.agent.delegate

import android.app.Activity
import android.content.Context
import android.preference.PreferenceManager
import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import org.json.JSONObject
import xyz.ummo.agent.R


abstract class AcceptDispatch(context: Context, user:String, product:String, agent:String, fare: String) {
    init {
        val data = JSONObject()
        data.put("user_id",user)
        data.put("product_id",product)
        data.put("agent_id", agent)
        data.put("fare", "150")

        Log.e("AcceptDispatch",data.toString())

        Fuel.post("${context.getString(R.string.serverUrl)}/api/dispatch-accept")
                .jsonBody(data.toString())
                .response { request, response, result ->
                    if(response.statusCode == 200){
                        ( context as Activity).runOnUiThread {
                            done(response.data,response.statusCode)
                        }
                    }
                }
    }

    abstract fun done(data:ByteArray,code:Int)
}