package xyz.ummo.agent.delegate

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import org.json.JSONObject

abstract class SelfDelegate(user_contact:String, user_name:String, product_id:String, agent_id:String) {
    init {
        val _body = JSONObject()
                .put("agent_id",agent_id)
                .put("product_id", product_id)
                .put("user", JSONObject()
                        .put("mobile_contact", user_contact)
                        .put("name", user_name))

        Fuel.post("/api/self-delegate")
                .jsonBody(_body.toString())
                .response { request, response, result ->
                    done(response.data, response.statusCode)
                }
    }

    abstract fun done(data: ByteArray, code: Number)
}