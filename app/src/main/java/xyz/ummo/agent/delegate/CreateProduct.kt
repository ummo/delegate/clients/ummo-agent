package xyz.ummo.agent.delegate

import android.util.Log
import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.extensions.jsonBody
import com.google.gson.Gson
import com.google.gson.JsonArray
import org.json.JSONArray
import org.json.JSONObject
import timber.log.Timber

abstract class CreateProduct(
        product_name:String,
        product_description:String,
        procurement_process:ArrayList<String>,
        presence_required:Boolean,
        procurement_cost:String,
        documents:ArrayList<String>,
        public_service:String,
        duration:String) {
    init {
        Log.e("CreateProduct.kt","Here")

        val _body = JSONObject()
                .put("product_name", product_name)
                .put("procurement_process",arrayToJson(procurement_process))
                .put("product_description",product_description)
                .put("duration",duration)
                .put("requirements",JSONObject()
                        .put("documents",arrayToJson(documents))
                        .put("procurement_cost",procurement_cost.toFloat())
                        .put("presence_required", presence_required))
                .put("public_service",public_service)
        Log.e("CreateProduct.kt","Body->$_body")

        Fuel.post("/product")
                .jsonBody(_body.toString())
                .response { request, response, result ->
                    if(response.statusCode==201){
                        done(response.data, response.statusCode)
                    }
                    Timber.e("Posting $_body; Response Data -> ${response.data}; Response Code -> ${response.statusCode}")
                }
    }

    fun arrayToJson(list:ArrayList<String>):JSONArray {
        val arr = JSONArray()
        list.forEach { arr.put(it) }
        return arr
    }

    abstract fun done(data:ByteArray,code:Number)
}

