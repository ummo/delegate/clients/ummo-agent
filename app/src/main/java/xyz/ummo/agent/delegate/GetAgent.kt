package xyz.ummo.agent.delegate

import android.util.Log
import com.github.kittinunf.fuel.Fuel
import timber.log.Timber

abstract class GetAgent {
    init {
        Fuel.get("/agent")
                .response { request, response, result ->
                    done(response.data, response.statusCode)
                    //TODO: [BUG] -> Investigate why this response code returns 403
                    Timber.e("Agent-> ${String(response.data)} Status Code-> ${response.statusCode}")
                }
    }

    abstract fun done(data:ByteArray,code:Number)
}