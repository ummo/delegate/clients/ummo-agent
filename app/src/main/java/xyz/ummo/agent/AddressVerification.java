package xyz.ummo.agent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import xyz.ummo.agent.ui.MainActivity;

public class AddressVerification extends AppCompatActivity implements View.OnClickListener{

    ImageView swipeCamera, startVideo, flashLight;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_address_verification);
        swipeCamera = findViewById(R.id.swipe_camera_icon);
        startVideo = findViewById(R.id.capture_image_icon);
        flashLight = findViewById(R.id.flash_light_icon);

        swipeCamera.setOnClickListener(this);
        startVideo.setOnClickListener(this);
        flashLight.setOnClickListener(this);
    }

    public void onClick(View v) {
        if (v == startVideo) {

            intentToMainScreen();

        } else if (v == flashLight) {
        }
    }

    private void intentToMainScreen(){

        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
        finish();
    }
}
